
#include <AsyncSpawnerSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace AsyncSpawner
{
    void AsyncSpawnerSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<AsyncSpawnerSystemComponent, AZ::Component>()
                ->Version(0);

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<AsyncSpawnerSystemComponent>("AsyncSpawner", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("System"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ;
            }
        }
    }

    void AsyncSpawnerSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("AsyncSpawnerService"));
    }

    void AsyncSpawnerSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("AsyncSpawnerService"));
    }

    void AsyncSpawnerSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
		required.push_back(AZ_CRC("SpawnerService"));
    }

    void AsyncSpawnerSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

    void AsyncSpawnerSystemComponent::Init()
    {
    }

    void AsyncSpawnerSystemComponent::Activate()
    {
        AsyncSpawnerRequestBus::Handler::BusConnect();
    }

    void AsyncSpawnerSystemComponent::Deactivate()
    {
        AsyncSpawnerRequestBus::Handler::BusDisconnect();
    }
}
