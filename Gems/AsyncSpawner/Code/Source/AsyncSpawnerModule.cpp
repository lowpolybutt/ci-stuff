
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <AsyncSpawnerSystemComponent.h>

namespace AsyncSpawner
{
    class AsyncSpawnerModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(AsyncSpawnerModule, "{6D1EECB0-A564-45E2-A5B4-5DFAFF432AA1}", AZ::Module);
        AZ_CLASS_ALLOCATOR(AsyncSpawnerModule, AZ::SystemAllocator, 0);

        AsyncSpawnerModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                AsyncSpawnerSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<AsyncSpawnerSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(AsyncSpawner_eff10773d90e4ff2b71f7c10f4fdf936, AsyncSpawner::AsyncSpawnerModule)
