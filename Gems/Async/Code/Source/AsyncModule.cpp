
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <AsyncSystemComponent.h>

namespace Async
{
    class AsyncModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(AsyncModule, "{55584522-ACCE-4873-9324-64168BC264C6}", AZ::Module);
        AZ_CLASS_ALLOCATOR(AsyncModule, AZ::SystemAllocator, 0);

        AsyncModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                AsyncSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<AsyncSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(Async_edec4e05b4a44faa9adea4dbed5d0717, Async::AsyncModule)
