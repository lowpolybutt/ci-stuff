#pragma once

#include <AzCore/Component/Component.h>

#include <Async/AsyncBus.h>

namespace Async
{
    class AsyncSystemComponent
        : public AZ::Component
        , protected AsyncRequestBus::Handler
    {
    public:
        AZ_COMPONENT(AsyncSystemComponent, "{AFB53BA1-A265-4E98-A4CF-CEBC63F2F319}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // AsyncRequestBus interface implementation
		void PrintEbusMessage() override;
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
