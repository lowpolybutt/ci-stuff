
#include <AsyncSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace Async
{
    void AsyncSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<AsyncSystemComponent, AZ::Component>()
                ->Version(0);

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<AsyncSystemComponent>("Async", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("System"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ;
            }
        }
    }

    void AsyncSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("AsyncService"));
    }

    void AsyncSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("AsyncService"));
    }

    void AsyncSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void AsyncSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

    void AsyncSystemComponent::Init()
    {
		AZ_Printf("EBUS", "%s", "Init Async")
    }

    void AsyncSystemComponent::Activate()
    {
        AsyncRequestBus::Handler::BusConnect();
		AZ_Printf("EBUS", "%s", "Activate Async")
    }

    void AsyncSystemComponent::Deactivate()
    {
        AsyncRequestBus::Handler::BusDisconnect();
    }

	void AsyncSystemComponent::PrintEbusMessage()
	{
		AZ_Printf("EBUS", "%s", "hi");
	}
}
