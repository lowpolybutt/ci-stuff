
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <SteelSeriesEngineSystemComponent.h>
#include <SteelSeriesEngineEntityComponent.h>

namespace SteelSeriesEngine
{
    class SteelSeriesEngineModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(SteelSeriesEngineModule, "{B5CF4435-3E7E-4558-BCDD-77BD15E31FED}", AZ::Module);
        AZ_CLASS_ALLOCATOR(SteelSeriesEngineModule, AZ::SystemAllocator, 0);

        SteelSeriesEngineModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                SteelSeriesEngineSystemComponent::CreateDescriptor(),
				SteelSeriesEngineEntityComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<SteelSeriesEngineSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(SteelSeriesEngine_ab5cb6c3ebe644f4a047b5f2fb67c34e, SteelSeriesEngine::SteelSeriesEngineModule)
