#pragma once

#include <AzCore/Component/Component.h>

#include <LmbrAWS/ILmbrSDKSupport.h>
#include <HttpRequestor/HttpRequestorBus.h>
#include <HttpRequestor/HttpRequestParameters.h>
#include <HttpRequestor/HttpTextRequestParameters.h>
#include <HttpRequestor/HttpTypes.h>

#include <SteelSeriesEngine/SteelSeriesEngineBus.h>

namespace SteelSeriesEngine
{
    class SteelSeriesEngineSystemComponent
        : public AZ::Component
        , protected SteelSeriesEngineRequestBus::Handler
    {
    public:
        AZ_COMPONENT(SteelSeriesEngineSystemComponent, "{7D71EE81-78A0-416C-B6A4-D96B1863E491}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

		const char* m_engineAddress;

    protected:
        ////////////////////////////////////////////////////////////////////////
        // SteelSeriesEngineRequestBus interface implementation
		const char* GetEngineAddress() override;
		void SendHeartbeat(std::string) override;
		void RegisterGame(GameRegistrationData) override;
		void RegisterEvent(EventRegistrationData) override;
		void SendGameEvent(GameEvent) override;
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////

	private:
		void GetAddressCallback(const AZStd::string& response, Aws::Http::HttpResponseCode respCode);
		void RegisterEventCallback(const AZStd::string response, Aws::Http::HttpResponseCode respCode);
    };
}
