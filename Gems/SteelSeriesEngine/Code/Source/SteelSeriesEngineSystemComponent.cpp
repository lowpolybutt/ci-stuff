#pragma warning(disable:4996)

#include <SteelSeriesEngineSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

#include <AzCore/JSON/filereadstream.h>
#include <AzCore/JSON/document.h>

#include <fstream>
#include <cstdio>

namespace SteelSeriesEngine
{
    void SteelSeriesEngineSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<SteelSeriesEngineSystemComponent, AZ::Component>()
                ->Version(0);

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<SteelSeriesEngineSystemComponent>("SteelSeriesEngine", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("System"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ;
            }
        }
    }

    void SteelSeriesEngineSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("SteelSeriesEngineService"));
    }

    void SteelSeriesEngineSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("SteelSeriesEngineService"));
    }

    void SteelSeriesEngineSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void SteelSeriesEngineSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

	const char* SteelSeriesEngineSystemComponent::GetEngineAddress()
	{
		FILE* fp = fopen("C:\\ProgramData\\SteelSeries\\SteelSeries Engine 3\\coreProps.json",
			"rb");

		char rb[65536];
		rapidjson::FileReadStream is(fp, rb, sizeof(rb));

		rapidjson::Document d;
		d.ParseStream(is);

		fclose(fp);

		m_engineAddress = d["address"].GetString();

		return d["address"].GetString();
	}

	void SteelSeriesEngineSystemComponent::SendHeartbeat(std::string game)
	{
		AZStd::map<AZStd::string, AZStd::string> headers;
		headers.insert("Content-Type", "application/json");
		HttpRequestor::HttpRequestorRequestBus::Broadcast(
			&HttpRequestor::HttpRequestorRequestBus::Events::AddTextRequestWithHeadersAndBody,
			AZStd::string::format("%s/game_heartbeat", m_engineAddress), 
			Aws::Http::HttpMethod::HTTP_POST,
			game, [this](AZStd::string&& d, Aws::Http::HttpResponseCode c)
			{
				return;
			}
		);
	}

	void SteelSeriesEngineSystemComponent::RegisterGame(GameRegistrationData data)
	{
		AZStd::map<AZStd::string, AZStd::string> headers;
		headers.insert("Content-Type", "application/json");
		HttpRequestor::HttpRequestorRequestBus::Broadcast(
			&HttpRequestor::HttpRequestorRequestBus::Events::AddRequestWithHeadersAndBody,
			AZStd::string::format("%s/game_metadata", m_engineAddress),
			Aws::Http::HttpMethod::HTTP_POST,
			data, nullptr
		);
	}

	void SteelSeriesEngineSystemComponent::RegisterEvent(EventRegistrationData data)
	{
		AZStd::map<AZStd::string, AZStd::string> headers;
		headers.insert("Content-Type", "application/json");
		HttpRequestor::HttpRequestorRequestBus::Broadcast(
			&HttpRequestor::HttpRequestorRequestBus::Events::AddRequestWithHeadersAndBody,
			AZStd::string::format("%s/register_game_event", m_engineAddress),
			Aws::Http::HttpMethod::HTTP_POST,
			data, nullptr
		);
	}

	void SteelSeriesEngineSystemComponent::SendGameEvent(GameEvent data)
	{
		AZStd::map<AZStd::string, AZStd::string> headers;
		headers.insert("Content-Type", "application/json");
		HttpRequestor::HttpRequestorRequestBus::Broadcast(
			&HttpRequestor::HttpRequestorRequestBus::Events::AddTextRequestWithHeadersAndBody,
			AZStd::string::format("%s/game_event", m_engineAddress), Aws::Http::HttpMethod::HTTP_POST,
			data, [this](AZStd::string&& d, Aws::Http::HttpResponseCode c)
			{
				return;
			}
		);
	}

    void SteelSeriesEngineSystemComponent::Init()
    {
    }

    void SteelSeriesEngineSystemComponent::Activate()
    {
        SteelSeriesEngineRequestBus::Handler::BusConnect();
    }

    void SteelSeriesEngineSystemComponent::Deactivate()
    {
        SteelSeriesEngineRequestBus::Handler::BusDisconnect();
    }

	void SteelSeriesEngineSystemComponent::GetAddressCallback(const AZStd::string & response, Aws::Http::HttpResponseCode respCode)
	{
		AZ_Printf("SSE3", "Received %s", response);
	}

	void SteelSeriesEngineSystemComponent::RegisterEventCallback(const AZStd::string response, Aws::Http::HttpResponseCode respCode)
	{

	}
}
