#pragma once

#include <AzCore/Component/Component.h>

#include <PlayerInputCapture/PlayerInputCaptureBus.h>

namespace PlayerInputCapture
{
    class PlayerInputCaptureSystemComponent
        : public AZ::Component
        , protected PlayerInputCaptureRequestBus::Handler
    {
    public:
        AZ_COMPONENT(PlayerInputCaptureSystemComponent, "{1A4847D5-44DB-430C-87BE-66F189693861}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // PlayerInputCaptureRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
