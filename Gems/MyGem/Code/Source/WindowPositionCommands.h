#pragma once

struct ISystem;
struct IConsoleCommandArgs;

namespace WindowPosition
{
	class WindowPositionCommands
	{
	public:
		void Register(ISystem&);
		void Unregister(ISystem&);
		static void WindowXY(IConsoleCommandArgs*);
	};
}