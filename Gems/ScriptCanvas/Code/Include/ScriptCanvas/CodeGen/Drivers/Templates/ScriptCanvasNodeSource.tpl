{#
All or portions of this file Copyright (c) Amazon.com, Inc. or its affiliates or 
its licensors.

For complete copyright and license terms please see the LICENSE at the root of this
distribution (the "License"). All use of this software is governed by the License,
or, if provided, by the license below or the license accompanying this file. Do not
remove or modify any license notices. This file is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// This code was produced with AzCodeGenerator, any modifications made will not be preserved.
// If you need to modify this code see:
//
// ScriptCanvas\CodeGen\Drivers\Templates\ScriptCanvasNodeSource.tpl
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Important! This generated source file assumes the file will be included into the .CPP file for which
// AzCodeGenerator was run.

#include <AzCore/RTTI/BehaviorContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/RTTI/TypeInfo.h>

#include <ScriptCanvas/Core/Slot.h>
#include <ScriptCanvas/Core/Datum.h>
#include <ScriptCanvas/Core/Attributes.h>
#include <ScriptCanvas/Core/Contracts.h>
#include <ScriptCanvas/Core/DatumBus.h>


{%- macro add_attribute(attribute, tags) -%}
{%- set value = tags[attribute] -%}
{%- if value is defined -%} 
->Attribute(AZ::Edit::Attributes::{{attribute}}, {{value}})
{%- endif -%}
{%- endmacro -%}

{%- macro CleanName(name) -%}
{{name|replace(' ','')|replace('"','')}}
{%- endmacro -%}

{%- macro RemoveRawQuotes(name) -%}
{%- if name is string and name.startswith('R"(') and name.endswith(')"') -%}
{{name[3:-2]}}
{%- else -%}
{{name}}
{%- endif -%}
{%- endmacro -%}

{% for class in json_object.objects if class.annotations.ScriptCanvas_Node is defined %}

    {% set bases = asArray(class.bases)|map(attribute='qualified_name')|list %}
    {% set version = 0 %}

    {% if class.annotations.ScriptCanvas_Node is defined and class.annotations.ScriptCanvas_Node.Version is defined %}
        {% set version = class.annotations.ScriptCanvas_Node.Version[0] %}
        {% if class.annotations.ScriptCanvas_Node.Version|length > 1 %}
            {% set converter = class.annotations.ScriptCanvas_Node.Version[1] %}
        {% endif %}
    {% endif %}

void {{class.qualified_name}}::ConfigureSlots()
{
    {%- set bases = asArray(class.bases)|map(attribute='qualified_name')|list %}
    {% if bases|length() > 0 %}    
    {{bases[0]}}::ConfigureSlots();
    {% endif %}

{%- for field in class.fields %}
{% if field.annotations.ScriptCanvas_Property is defined %}

    {% set input = False -%}
    {% set output = False -%}
    {% if field.annotations.ScriptCanvas_Property.Input is not defined and field.annotations.ScriptCanvas_Property.Output is not defined %}
        {% set input = True -%}
        {% set output = True -%}
    {% else %}
        {% if field.annotations.ScriptCanvas_Property.Input is defined %}
            {% set input = True -%}
        {% endif %}
        {% if field.annotations.ScriptCanvas_Property.Output is defined %}
            {% set output = True -%}
        {% endif %}
    {% endif %}
    {%- if input == True %}
    // {{field.annotations.ScriptCanvas_Property.Name[0]|replace('"', '')}}
    {
        {% if field.annotations.ScriptCanvas_Property.Overloaded is defined %}
        AddInputDatumOverloadedSlot("{{field.annotations.ScriptCanvas_Property.Name[0]|replace('"', '')}}");
        {% elif field.annotations.DefaultValue is defined %}
            {% set defaultValue = field.annotations.DefaultValue|replace('"', '') %}
        AddInputDatumSlot("{{field.annotations.ScriptCanvas_Property.Name[0]|replace('"', '')}}", "{{field.annotations.ScriptCanvas_Property.Name[1]|replace('"', '')}}", ScriptCanvas::Datum::eOriginality::Original, {{field.type}}({{defaultValue}}));
        {% else %}
        AddInputDatumSlot("{{field.annotations.ScriptCanvas_Property.Name[0]|replace('"', '')}}", "{{field.annotations.ScriptCanvas_Property.Name[1]|replace('"', '')}}", AZStd::move(ScriptCanvas::Data::FromAZType(azrtti_typeid<{{field.type}}>())), ScriptCanvas::Datum::eOriginality::Original);
        {% endif %}
    }
    {%- endif -%}
    {%- if output == True %}
        AddOutputTypeSlot("{{field.annotations.ScriptCanvas_Property.Name[0]|replace('"', '')}}", "{{field.annotations.ScriptCanvas_Property.Name[1]|replace('"', '')}}", AZStd::move(ScriptCanvas::Data::FromAZType(azrtti_typeid<{{field.type}}>())){% if field.annotations.ScriptCanvas_Property.OutputStorageSpec is defined %},OutputStorage::Optional{% endif %});


    {%- endif -%}
{% endif %}
{% if field.annotations.ScriptCanvas_In is defined %}
    // {{field.annotations.ScriptCanvas_In.Name[0]|replace('"', '')}}
    {
        AddSlot({"{{field.annotations.ScriptCanvas_In.Name[0]|replace('"', '')}}", "{{field.annotations.ScriptCanvas_In.Name[1]|replace('"', '')}}", ScriptCanvas::SlotType::ExecutionIn,
            AZStd::vector<ScriptCanvas::ContractDescriptor>{
                {# ADD CONTRACTS #}
                {%- if field.annotations.ScriptCanvas_In.Contracts is defined -%}
                    {%- if field.annotations.ScriptCanvas_In.Contracts is mapping %}
                        {%- set contracts = (field.annotations.ScriptCanvas_In.Contracts) %}
                    {%- else %}
                        {%- set contracts = asArray(field.annotations.ScriptCanvas_In.Contracts) %}
                    {% endif %}

                {%- for contract in contracts %}
                // Contract: {{contract}}
                { []() { return aznew {{contract}}{% if contract is mapping %}({%for c in contract%}{{c}}{{ "," if not loop.last else "" }}{%endfor%}){% endif %}; } }{{"," if not loop.last else ""}}
            {% endfor %}
        {% endif %}
        }});
    }
{% endif %}
{% if field.annotations.ScriptCanvas_Out is defined %}
    // {{field.annotations.ScriptCanvas_Out.Name[0]|replace('"', '')}}
    {
        AddSlot("{{field.annotations.ScriptCanvas_Out.Name[0]|replace('"', '')}}", "{{field.annotations.ScriptCanvas_Out.Name[1]|replace('"', '')}}", ScriptCanvas::SlotType::ExecutionOut);
    }
{% endif %}
{% if field.annotations.ScriptCanvas_OutLatent is defined %}
    // {{field.annotations.ScriptCanvas_OutLatent.Name[0]|replace('"', '')}}
    {
        AddSlot("{{field.annotations.ScriptCanvas_OutLatent.Name[0]|replace('"', '')}}", "{{field.annotations.ScriptCanvas_OutLatent.Name[1]|replace('"', '')}}", ScriptCanvas::SlotType::LatentOut);
    }
{% endif %}
{% endfor %}
}

bool {{class.qualified_name}}::IsEntryPoint() const { return {% if class.annotations.ScriptCanvas_Node.GraphEntryPoint is defined and class.annotations.ScriptCanvas_Node.GraphEntryPoint == "true" %}true{% else %}false{% endif %}; }

void {{class.qualified_name}}::Reflect(AZ::ReflectContext* context)
{
    {% set bases = asArray(class.bases)|map(attribute='qualified_name')|list %}
    {% if bases|length() > 0 %}
    AZ_STATIC_ASSERT((std::is_base_of<ScriptCanvas::Node, {{bases[0]}}>::value), "Script Canvas nodes require the first base class to be derived from ScriptCanvas::Node");
    {% endif %}
    {% if bases|length() > 1 %}
    {% set bases = [class.bases[0].qualified_name] %}
    {% endif %}
    {% if class.annotations.ScriptCanvas_Node.BaseClass is defined %}
    {% set bases = bases + asArray(class.annotations.ScriptCanvas_Node.BaseClass) %}
    {% endif %}

    if (AZ::SerializeContext* serializeContext = azrtti_cast<AZ::SerializeContext*>(context))
    {
        serializeContext->Class<{{class.qualified_name}}{% if bases|length() > 0 %}, {% for base in bases %} {{ base|replace('"','') }} {% if not loop.last %}, {% endif %}{% endfor %}{% endif %}>()
        {% if class.annotations.ScriptCanvas_Node.EventHandler is defined %}
            ->EventHandler<{{class.annotations.ScriptCanvas_Node.EventHandler|replace('"', '')}}>()
        {% endif %}
        {% if version is defined %}
            ->Version({{version}}{% if converter is defined %}, &{{converter}}{% endif %})
        {% endif %}
        {% for field in class.fields %}
            {% if field.annotations.SerializedProperty is defined %}
                ->Field("{{field.name}}", &{{class.name}}::{{field.name}})
            {% endif %}
        {% endfor %}
        ;

        if (AZ::EditContext* editContext = serializeContext->GetEditContext())
        {
            {% set preferredClassName = '"%s"'|format(class.name) %}
            {% if class.annotations.ScriptCanvas_Node is defined %}
                {% if class.annotations.ScriptCanvas_Node.Name is defined %}
                    {% set preferredClassName = asArray(class.annotations.ScriptCanvas_Node.Name)[0] %}
                {% endif %}
            {% endif %}
            {# ----------- Get the node description ----------- #}
            {% set nodeDescription = "" %}
            {% if class.annotations.ScriptCanvas_Node is defined and class.annotations.ScriptCanvas_Node.Description is defined %}
                {% set nodeDescription = class.annotations.ScriptCanvas_Node.Description %}
            {% elif class.annotations.ScriptCanvas_Node.Name is defined and class.annotations.ScriptCanvas_Node.Name|length() > 1 %}
                {% set nodeDescription = class.annotations.ScriptCanvas_Node.Name[1] %}
            {% endif %}
            {% if class.annotations.ScriptCanvas_Node.Deprecated is defined and class.annotations.ScriptCanvas_Node.Deprecated|length > 0 %}
            {%- set nodeDescription = class.annotations.ScriptCanvas_Node.Deprecated %}
            {% endif %}
            editContext->Class<{{class.qualified_name}}>({{preferredClassName}}, {{nodeDescription}})
                       ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                            ->Attribute(AZ::Edit::Attributes::Visibility, AZ::Edit::PropertyVisibility::ShowChildrenOnly)
                       {% if class.annotations.ScriptCanvas_Node.Category is defined %}
                            ->Attribute(AZ::Edit::Attributes::Category, {{class.annotations.ScriptCanvas_Node.Category}})
                       {% endif %}
                            ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                        {% if class.annotations.ScriptCanvas_Node is defined and class.annotations.ScriptCanvas_Node.Icon is defined %}
                            ->Attribute(AZ::Edit::Attributes::Icon, {{class.annotations.ScriptCanvas_Node.Icon}})
                        {% endif %}
                        {% if class.annotations.ScriptCanvas_Node.Deprecated is defined %}
                            ->Attribute(ScriptCanvas::Attributes::Node::TitlePaletteOverride, "DeprecatedNodeTitlePalette")
                            ->Attribute(AZ::Script::Attributes::Deprecated, true)
                        {% else %}
                        {% if class.annotations.ScriptCanvas_Node is defined and class.annotations.ScriptCanvas_Node.EditAttributes is mapping %}
                        {% for editAttributeKey, editAttributeValue in class.annotations.ScriptCanvas_Node.EditAttributes.items() %}
                            ->Attribute({{RemoveRawQuotes(editAttributeKey)}}, {{editAttributeValue}})
                        {% endfor %}
                        {% endif %}
                        {% endif %}
      
                        {% for field in class.fields -%}
                        {% set property = field.annotations.EditProperty %}
                            {% if property is defined %}

                                    {%- set uihandler = 'AZ::Edit::UIHandlers::Default' -%}
                                    {%- if property.UIHandler is defined %}
                                        {% set uihandler = property.UIHandler %}
                                    {% endif -%}
                                    {%- if property.SerializeProperty is defined %}
                                    ->DataElement({{uihandler}}, &{{class.name}}::{{property.SerializeProperty}}, "{{property.Name[0]|replace('"','')}}", "")
                                    {%- elif property.Name is defined %}
                                    ->DataElement({{uihandler}}, &{{class.name}}::{{field.name}}, {{property.Name[0]}}, {% if property.Name|length > 1 %}{{property.Name[1]}} {% else %} ""{% endif %})
                                    {%- else %}
                                    ->DataElement({{uihandler}}, &{{class.name}}::{{field.name}}, "{{field.name}}", "")
                                    {%- endif %}

                                    {{- add_attribute("Category", property) }} 
                                    {{- add_attribute("ChangeNotify", property) }} 
                                    {{- add_attribute("AutoExpand", property) }}
                                    {{- add_attribute("Visibility", property) }}
                                    {{- add_attribute("DescriptionTextOverride", property) }}
                                    {{- add_attribute("NameLabelOverride", property) }}
                                    {{- add_attribute("Min", property) }}
                                    {{- add_attribute("Max", property) }}
                                    {%+ if property.EditAttributes is mapping %}
                                    {% for editAttributeKey, editAttributeValue in property.EditAttributes.items() %}
                                        {% set attribs = asArray(editAttributeValue) %}
                                        ->Attribute({{RemoveRawQuotes(editAttributeKey)}}, {% for attrib in attribs %}{{attrib}}{%if not loop.last %}, {% endif %}{% endfor %})
                                    {% endfor %}
                                    {% endif %}
                            {% endif %}
                        {% endfor %}
                        ;
        }
        
    }
}

{# Property Getters #}
{%+ for field in class.fields -%}
    {%+ if field.annotations.ScriptCanvas_Property is defined -%}
        {{field.type}} {{class.name}}Property::Get{{CleanName(field.annotations.ScriptCanvas_Property.Name[0])}}(ScriptCanvas::Node* owner)
        {
            ScriptCanvas::SlotId slotId = Get{{CleanName(field.annotations.ScriptCanvas_Property.Name[0])}}SlotId(owner);
            const ScriptCanvas::Datum* datum = {{class.qualified_name}}::GetInput(*owner, slotId);
            if(!datum)
            {
                AZ_Error("Script Canvas", false, "Cannot find generated code gen slot with name {{CleanName(field.annotations.ScriptCanvas_Property.Name[0])}}. Has the ScriptCanvas_Property::Name changed without writing a version converter");
            }
            const {{field.type}}* datumValue = datum ? datum->GetAs<{{field.type}}>() : nullptr;
            return datumValue ? *datumValue : {{field.type}}();
        }

    {% endif -%}
{%- endfor -%}

{% endfor %}

{# To see the information available during code generation uncomment the template comment block below. #}

// Reference JSON object
/* 
{{json_str}}
*/

