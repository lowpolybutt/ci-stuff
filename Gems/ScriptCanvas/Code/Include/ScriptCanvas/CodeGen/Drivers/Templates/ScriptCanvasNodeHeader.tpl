{#
All or portions of this file Copyright (c) Amazon.com, Inc. or its affiliates or
its licensors.

For complete copyright and license terms please see the LICENSE at the root of this
distribution (the "License"). All use of this software is governed by the License,
or, if provided, by the license below or the license accompanying this file. Do not
remove or modify any license notices. This file is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// This code was produced with AzCodeGenerator, any modifications made will not be preserved.
// If you need to modify this code see:
//
// ScriptCanvas\CodeGen\Drivers\Templates\ScriptCanvasNodeHeader.tpl
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// This generation assumes the file will be included within the class and included into the source header.

#pragma once

#include <ScriptCanvas/Core/Datum.h>
#include <ScriptCanvas/Core/Node.h>

{% for class in json_object.objects %}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
////
//// {{class.name}}
////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

{% if class.annotations.ScriptCanvas_Node is defined %}
    {%- if class.annotations.ScriptCanvas_Node.Uuid is defined %}
        {%- set uuid = class.annotations.ScriptCanvas_Node.Uuid %}
        {%- set bases = asArray(class.bases)|map(attribute='qualified_name')|list %}
// The following will be injected directly into the source header file for which AzCodeGenerator is being run.
// You must #include the generated header into the source header
#define AZ_GENERATED_{{ asStringIdentifier(class.annotations.Identifier) }} \
public: \
    AZ_COMPONENT({{class.name}}, {{uuid}}{% if bases|length() > 0 %}, {{ bases[0] }}{% endif %}); \
    static void Reflect(AZ::ReflectContext* reflection); \
    void ConfigureSlots() override; \
    bool IsEntryPoint() const override; \
    using Node::GetInput; \
protected:\
    void Visit(ScriptCanvas::NodeVisitor& visitor) const override { visitor.Visit(*this); }\
public:\
{% if class.fields|length > 0 %}
    friend struct ::{{class.name}}Property;
{% endif %}

    {% else %}

        {% raise "'%s' - 'ScriptCanvas_Node' requires uuid ScriptCanvas_Node::Uuid('{XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX})'"|format(class.qualified_name) %}

    {% endif %}

{%- macro CleanName(name) -%}
{{name|replace(' ','')|replace('"','')}}
{%- endmacro -%}

{% if class.fields|length > 0 %}
// Helpers for easily accessing properties and slots
struct {{class.name}}Property
{
{%- for field in class.fields %}
{%- if field.annotations.ScriptCanvas_Out is defined %}
    {%if field.annotations.ScriptCanvas_Out.Name[1]|wordcount > 0 %}    // {{field.annotations.ScriptCanvas_Out.Name[1]|replace('"', '')}}{% endif %}

    static ScriptCanvas::SlotId Get{{CleanName(field.annotations.ScriptCanvas_Out.Name[0])}}SlotId (ScriptCanvas::Node* owner)
    {
        return owner->GetSlotId("{{field.annotations.ScriptCanvas_Out.Name[0]|replace('"', '')}}");
    }
{% endif %}
{%- if field.annotations.ScriptCanvas_OutLatent is defined %}

    {%if field.annotations.ScriptCanvas_OutLatent.Name[1]|wordcount > 0 %}    // {{field.annotations.ScriptCanvas_OutLatent.Name[1]|replace('"', '')}}{% endif %}

    static ScriptCanvas::SlotId Get{{CleanName(field.annotations.ScriptCanvas_OutLatent.Name[0])}}SlotId (ScriptCanvas::Node* owner)
    {
        return owner->GetSlotId("{{field.annotations.ScriptCanvas_OutLatent.Name[0]|replace('"', '')}}");
    }
{% endif %}
{%- if field.annotations.ScriptCanvas_In is defined %}

    {%if field.annotations.ScriptCanvas_In.Name[1]|wordcount > 0 %}    // {{field.annotations.ScriptCanvas_In.Name[1]|replace('"', '')}}{% endif %}

    static ScriptCanvas::SlotId Get{{CleanName(field.annotations.ScriptCanvas_In.Name[0])}}SlotId (ScriptCanvas::Node* owner)
    {
        return owner->GetSlotId("{{field.annotations.ScriptCanvas_In.Name[0]|replace('"', '')}}");
    }
{% endif %}
{%- if field.annotations.ScriptCanvas_Property is defined %}

    {%if field.annotations.ScriptCanvas_Property.Name[1]|wordcount > 0 %}    // {{field.annotations.ScriptCanvas_Property.Name[1]|replace('"', '')}}{% endif %}

    static ScriptCanvas::SlotId Get{{CleanName(field.annotations.ScriptCanvas_Property.Name[0])}}SlotId (ScriptCanvas::Node* owner)
    {
        return owner->GetSlotId("{{field.annotations.ScriptCanvas_Property.Name[0]|replace('"', '')}}");
    }

    // See Gems/ScriptCanvas/Code/Include/ScriptCanvas/CodeGen/Drivers/Templates/ScriptCanvasNodeSource.tpl for implementation source
    static {{field.type}} Get{{CleanName(field.annotations.ScriptCanvas_Property.Name[0])}}(ScriptCanvas::Node* owner);
{% endif %}
{% endfor -%}
};
    {% endif %}
{% endif %}
{% endfor %}


{# To see the information available during code generation uncomment the template comment block below. #}
{#
// Reference JSON object
/*
{{json_str}}
*/
#}