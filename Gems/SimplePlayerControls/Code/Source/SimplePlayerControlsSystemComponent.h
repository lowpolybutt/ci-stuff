#pragma once

#include <AzCore/Component/Component.h>
#include <AzCore/Component/TickBus.h>
#include <AzCore/Component/TransformBus.h>

#include <AzCore/Math/Vector3.h>

#include <AzFramework/Physics/CharacterBus.h>

#include <SimplePlayerControls/SimplePlayerControlsBus.h>

namespace SimplePlayerControls
{
    class SimplePlayerControlsSystemComponent
        : public AZ::Component
		, public AZ::TickBus::Handler
        , protected SimplePlayerControlsRequestBus::Handler
    {
    public:
        AZ_COMPONENT(SimplePlayerControlsSystemComponent, "{AD6D3D2B-22A9-4B63-B9F0-7D85B7604122}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // SimplePlayerControlsRequestBus interface implementation
		void MoveX(float) override;
		void MoveY(float) override;
		void OnTick(float, AZ::ScriptTimePoint) override;
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////

		float m_moveScale = 10.f;

	private:
		AZ::Vector3 m_lastKnownPosition;
    };
}
