#pragma once

#include <AzCore/Component/Component.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>

#include <SimplePlayerControls/SimplePlayerControlsBus.h>

#include <AzFramework/Input/Events/InputChannelEventListener.h>
#include <AzFramework/Input/Devices/Keyboard/InputDeviceKeyboard.h>

namespace SimplePlayerControls
{
	class CMovement
		: public AZ::Component
		, public AzFramework::InputChannelEventListener
	{
	public:
		AZ_COMPONENT(CMovement, "{D19E2EDD-8F8F-4F9C-92FC-AB6F886D1AEB}");

		static void Reflect(AZ::ReflectContext*);

	protected:
		void Init() override;
		void Activate() override;
		void Deactivate() override;

		bool OnInputChannelEventFiltered(const AzFramework::InputChannel&) override;
	};
}