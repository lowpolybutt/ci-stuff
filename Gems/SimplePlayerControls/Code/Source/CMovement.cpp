#include "CMovement.h"

namespace SimplePlayerControls
{
	void CMovement::Reflect(AZ::ReflectContext* c)
	{
		if (AZ::SerializeContext* s = azrtti_cast<AZ::SerializeContext*>(c))
		{
			s->Class<SimplePlayerControls::CMovement, AZ::Component>()
				->Version(0)
				;

			if (AZ::EditContext* e = s->GetEditContext())
			{
				e->Class<CMovement>("CMovement", "Captures input")
					->ClassElement(AZ::Edit::ClassElements::EditorData, "")
					->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
					->Attribute(AZ::Edit::Attributes::AutoExpand, true)
					;
			}
		}
	}

	void CMovement::Init()
	{}

	void CMovement::Activate()
	{
		AzFramework::InputChannelEventListener::BusConnect();
	}

	void CMovement::Deactivate()
	{}

	bool CMovement::OnInputChannelEventFiltered(const AzFramework::InputChannel& channel)
	{
		const AzFramework::InputDeviceId deviceId = channel.GetInputDevice().GetInputDeviceId();
		bool bIsMovingForward = false;
		if (deviceId == AzFramework::InputDeviceKeyboard::Id)
		{
			const AzFramework::InputChannelId inputType = channel.GetInputChannelId();

			if (inputType == AzFramework::InputDeviceKeyboard::Key::AlphanumericW)
			{
				if (!bIsMovingForward)
				{
					SimplePlayerControlsRequestBus::Broadcast(
						&SimplePlayerControlsRequestBus::Events::MoveY,
						.0f
					);
					bIsMovingForward = true;
				} 
			}
			else
			{
				bIsMovingForward = false;
			}
		}
		return false;
	}
}