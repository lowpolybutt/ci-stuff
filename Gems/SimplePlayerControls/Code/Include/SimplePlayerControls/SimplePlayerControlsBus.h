#pragma once

#include <AzCore/EBus/EBus.h>

namespace SimplePlayerControls
{
    class SimplePlayerControlsRequests
        : public AZ::EBusTraits
    {
    public:
        //////////////////////////////////////////////////////////////////////////
        // EBusTraits overrides
        static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
        static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::Single;
        //////////////////////////////////////////////////////////////////////////

        // Put your public methods here
		virtual void MoveX(float) = 0;
		virtual void MoveY(float) = 0;
    };
    using SimplePlayerControlsRequestBus = AZ::EBus<SimplePlayerControlsRequests>;
} // namespace SimplePlayerControls
