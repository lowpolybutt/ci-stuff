
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <AsyncMinionSpawnerSystemComponent.h>

namespace AsyncMinionSpawner
{
    class AsyncMinionSpawnerModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(AsyncMinionSpawnerModule, "{415DB7D9-092E-405F-A03B-8371D65DE145}", AZ::Module);
        AZ_CLASS_ALLOCATOR(AsyncMinionSpawnerModule, AZ::SystemAllocator, 0);

        AsyncMinionSpawnerModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                AsyncMinionSpawnerSystemComponent::CreateDescriptor(),
            });
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(AsyncMinionSpawner_9aa96617823f44ebb350637fdfba329f, AsyncMinionSpawner::AsyncMinionSpawnerModule)
