#pragma once

#include <AzCore/EBus/EBus.h>
#include <AzCore/Component/EntityId.h>

namespace AsyncMinionSpawner
{
	class AsyncSpawnerFunnelBus
		: public AZ::EBusTraits
	{
	public:
		const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
		const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::ByIdAndOrdered;

		using BusIdType = AZ::EntityId;
		using BusIdOrderCompare = AZStd::greater<BusIdType>;
		using MutexType = AZStd::mutex;

		virtual void SpawnEntity(AZ::EntityId) = 0;
	};
	using AsyncSpawnerFunnelBusRequestsBus = AZ::EBus<AsyncSpawnerFunnelBus>;
}