
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <BaseMinionSystemComponent.h>

namespace BaseMinion
{
    class BaseMinionModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(BaseMinionModule, "{A6FB50F7-24CF-42B9-936A-B9E8C7E08086}", AZ::Module);
        AZ_CLASS_ALLOCATOR(BaseMinionModule, AZ::SystemAllocator, 0);

        BaseMinionModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                BaseMinionSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<BaseMinionSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(BaseMinion_cc474bbd36f840d1869ccbba0c142e78, BaseMinion::BaseMinionModule)
