#pragma once

#include <AzCore/Component/Component.h>

#include <BaseMinion/BaseMinionBus.h>

namespace BaseMinion
{
    class BaseMinionSystemComponent
        : public AZ::Component
        , protected BaseMinionRequestBus::Handler
    {
    public:
        AZ_COMPONENT(BaseMinionSystemComponent, "{08847435-C396-4CEC-A2B6-9A77544E58A0}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // BaseMinionRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
