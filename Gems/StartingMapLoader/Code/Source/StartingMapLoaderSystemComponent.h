#pragma once

#include <AzCore/Component/Component.h>
#include <AzCore/Component/TickBus.h>
#include <AzFramework/Components/ConsoleBus.h>

#include <StartingMapLoader/StartingMapLoaderBus.h>

namespace StartingMapLoader
{
    class StartingMapLoaderSystemComponent
        : public AZ::Component
		, public AZ::SystemTickBus::Handler
        , protected StartingMapLoaderRequestBus::Handler
    {
    public:
        AZ_COMPONENT(StartingMapLoaderSystemComponent, "{F8E09823-0D75-44C8-BEAB-028A104FC452}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // StartingMapLoaderRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////

		////////////////////////////////////////////////////////////////////////
		// AZ::SystemTickBus interface impl
		void OnSystemTick() override;
		////////////////////////////////////////////////////////////////////////

	private:
		AZStd::string m_clientMap;
		AZStd::string m_serverMap;
    };
}
