#pragma once

#include <AzCore/EBus/EBus.h>

namespace NetworkedPlayerController
{
    class NetworkedPlayerControllerRequests
        : public AZ::EBusTraits
    {
    public:
        //////////////////////////////////////////////////////////////////////////
        // EBusTraits overrides
        static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
        static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::ById;
        //////////////////////////////////////////////////////////////////////////

		enum class ActionState 
		{
			ActionStarted,
			ActionStopped
		};

        // Put your public methods here
		// Move Player forward with EMotion
		virtual void MoveForward(ActionState) = 0;
		// Move Player backward with EMotion
		virtual void MoveBackward(ActionState) = 0;
		// Move Player left with EMotion
		virtual void StrafeLeft(ActionState) = 0;
		// Move Player right with EMotion
		virtual void StrafeRight(ActionState) = 0;

		// Look horizontally
		virtual void RotateZ() = 0;
		// Look vertically
		virtual void RotateX() = 0;

		// Currently unused, rolls along Y axis
		virtual void RotateY() {};

    };
    using NetworkedPlayerControllerRequestBus = AZ::EBus<NetworkedPlayerControllerRequests>;
} // namespace NetworkedPlayerController
