#pragma once

#include <AzCore/Component/Component.h>

#include <NetworkedPlayerController/NetworkedPlayerControllerBus.h>

namespace NetworkedPlayerController
{
    class NetworkedPlayerControllerSystemComponent
        : public AZ::Component
        , protected NetworkedPlayerControllerRequestBus::Handler
    {
    public:
        AZ_COMPONENT(NetworkedPlayerControllerSystemComponent, "{0DB1B859-D529-4052-87E2-7AD24CE106E8}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // NetworkedPlayerControllerRequestBus interface implementation
		void MoveForward(ActionState) override {}
		void MoveBackward(ActionState) override {}
		void StrafeLeft(ActionState) override {}
		void StrafeRight(ActionState) override {}
		void RotateZ() override {}
		void RotateX() override {}
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
