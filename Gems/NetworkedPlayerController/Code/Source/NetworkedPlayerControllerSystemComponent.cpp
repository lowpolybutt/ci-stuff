
#include <NetworkedPlayerControllerSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace NetworkedPlayerController
{
    void NetworkedPlayerControllerSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<NetworkedPlayerControllerSystemComponent, AZ::Component>()
                ->Version(0);

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<NetworkedPlayerControllerSystemComponent>("NetworkedPlayerController", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("System"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ;
            }
        }
    }

    void NetworkedPlayerControllerSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("NetworkedPlayerControllerService"));
    }

    void NetworkedPlayerControllerSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("NetworkedPlayerControllerService"));
    }

    void NetworkedPlayerControllerSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void NetworkedPlayerControllerSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

    void NetworkedPlayerControllerSystemComponent::Init()
    {
    }

    void NetworkedPlayerControllerSystemComponent::Activate()
    {
		NetworkedPlayerControllerRequestBus::Handler::BusConnect(GetEntityId());
    }

    void NetworkedPlayerControllerSystemComponent::Deactivate()
    {
        NetworkedPlayerControllerRequestBus::Handler::BusDisconnect();
    }
}
