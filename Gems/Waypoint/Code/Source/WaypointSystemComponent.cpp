
#include <WaypointSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>


namespace Waypoint
{
    void WaypointSystemComponent::Reflect(AZ::ReflectContext* context)
    {
		AZ::SerializeContext* sc;
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
			sc = serialize;
			serialize->Class<WaypointSystemComponent, AZ::Component>()
				->Version(0)
				->Field("WaypointCastVector", &WaypointSystemComponent::m_positionToCast)
				->Field("WaypointCastDirectionVector", &WaypointSystemComponent::m_directionToCast)
				->Field("WaypointDirectionToTurn", &WaypointSystemComponent::m_directionToDirect)
				;

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<WaypointSystemComponent>("Waypoint", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
						->Attribute(AZ::Edit::Attributes::Category, "TowerDefence")
					->DataElement(AZ::Edit::UIHandlers::Vector3, &WaypointSystemComponent::m_positionToCast, "Waypoint Cast Vector", "The Vector to cast the Waypoint ray from")
					->DataElement(AZ::Edit::UIHandlers::Vector3, &WaypointSystemComponent::m_directionToCast, "Waypoint Cast Direction Vector", "The Direction to cast the Ray")
					->DataElement(AZ::Edit::UIHandlers::Vector3, &WaypointSystemComponent::m_directionToDirect, "Waypoint Direction to Turn", "The direction to tell the minion to turn")
                    ;
            }
        }
    }

    void WaypointSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("WaypointService"));
    }

    void WaypointSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("WaypointService"));
    }

    void WaypointSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void WaypointSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

    void WaypointSystemComponent::Init()
    {
		AZ_Printf("WPSC", "Hi I'm %s", GetEntityId().ToString());
		idsToIgnore.push_back(GetEntityId());
    }

    void WaypointSystemComponent::Activate()
    {
        WaypointRequestBus::Handler::BusConnect();
		AZ::TickBus::Handler::BusConnect();
    }

    void WaypointSystemComponent::Deactivate()
    {
		AZ::TickBus::Handler::BusDisconnect();
        WaypointRequestBus::Handler::BusDisconnect();
    }

	void WaypointSystemComponent::OnTick(float, AZ::ScriptTimePoint)
	{
		AzFramework::PhysicsSystemRequests::RayCastConfiguration conf;
		conf.m_origin = m_positionToCast;
		conf.m_direction = m_directionToCast;
		conf.m_maxDistance = 200.f;
		conf.m_maxHits = 10;
		conf.m_ignoreEntityIds = idsToIgnore;
		AzFramework::PhysicsSystemRequests::RayCastResult res;
		AzFramework::PhysicsSystemRequestBus::BroadcastResult(res, &AzFramework::PhysicsSystemRequestBus::Events::RayCast, conf);

		size_t hits = res.GetHitCount();
		if (hits > 0)
		{
			const AzFramework::PhysicsSystemRequestBus::Handler::RayCastHit* first = res.GetHit(0);
			TDMinionBase::TDMinionBaseRequestBus::Event(first->m_entityId,
				&TDMinionBase::TDMinionBaseRequestBus::Events::OnRayHit, m_directionToDirect, 0.f);
			AZ_Printf("WPSC", "Hit entity %s, %s: %f, %f, %f", first->m_entityId.ToString(), "in direction", (float)m_directionToCast.GetX(), (float)m_directionToCast.GetY(), (float)m_directionToCast.GetZ());
			idsToIgnore.push_back(first->m_entityId);
		}
	}
}
