
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <WaypointSystemComponent.h>

namespace Waypoint
{
    class WaypointModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(WaypointModule, "{DDBA5041-D7FF-4375-B266-0059A00BAA1C}", AZ::Module);
        AZ_CLASS_ALLOCATOR(WaypointModule, AZ::SystemAllocator, 0);

        WaypointModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                WaypointSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        //AZ::ComponentTypeList GetRequiredSystemComponents() const override
        //{
        //    return AZ::ComponentTypeList{
        //        azrtti_typeid<WaypointSystemComponent>(),
        //    };
        //}
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(Waypoint_38b16cfbad264a6c9a6353c34e08246f, Waypoint::WaypointModule)
