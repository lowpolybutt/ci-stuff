
#include <ProjectorSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace Projector
{
    void ProjectorSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<ProjectorSystemComponent, AZ::Component>()
                ->Version(0);

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<ProjectorSystemComponent>("Projector", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ;
            }
        }
    }

    void ProjectorSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("ProjectorService"));
    }

    void ProjectorSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("ProjectorService"));
    }

    void ProjectorSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void ProjectorSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

	void ProjectorSystemComponent::Move(AZ::Vector2 v)
	{
		if (!bIsEntitySpawned) return;
		AZ::Vector3 newPos{ (m_lastPosition.GetX() + v.GetX()), (m_lastPosition.GetY() + v.GetY()), 38.f };
		AZ_Printf("PSC", "%s: %f %f %f", "Moving to", (float)newPos.GetX(), (float)newPos.GetY(), 38.f);
		/*AZ_Printf("PSC", "%s: %fx %fy", "Received", (float)v.GetX(), (float)v.GetY());*/
		bool bIsStatic = true;
		AZ::TransformBus::EventResult(bIsStatic, GetEntityId(), &AZ::TransformBus::Events::IsStaticTransform);
		if (bIsStatic)
		{
			AZ_Printf("PSC", "%s", "Calling entity is static");
		}
		else
		{
			AZ::TransformBus::Event(GetEntityId(), &AZ::TransformBus::Events::SetWorldTranslation, newPos);
			AZ_Printf("PSC", "%s", "Calling entity is movable");
		}
	}

	void ProjectorSystemComponent::SetEntitySpawned(bool b)
	{
		bIsEntitySpawned = b;
	}

    void ProjectorSystemComponent::Init()
    {
    }

    void ProjectorSystemComponent::Activate()
    {
        ProjectorRequestBus::Handler::BusConnect();
    }

    void ProjectorSystemComponent::Deactivate()
    {
        ProjectorRequestBus::Handler::BusDisconnect();
    }
}
