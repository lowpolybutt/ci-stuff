#pragma once

#include <AzCore/Component/Component.h>
#include <AzCore/Component/TransformBus.h>

#include <Projector/ProjectorBus.h>

namespace Projector
{
    class ProjectorSystemComponent
        : public AZ::Component
        , protected ProjectorRequestBus::Handler
    {
    public:
        AZ_COMPONENT(ProjectorSystemComponent, "{B6FFB828-479D-4D15-9A90-1C2D130B32B9}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // ProjectorRequestBus interface implementation
		void Move(AZ::Vector2) override;
		void SetEntitySpawned(bool) override;
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////

	private:
		AZ::Vector3 m_lastPosition{ 500.f, 500.f, 38.f };
		bool bIsEntitySpawned = false;
    };
}
