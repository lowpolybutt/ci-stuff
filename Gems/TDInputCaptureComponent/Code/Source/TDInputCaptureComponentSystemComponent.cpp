
#include <TDInputCaptureComponentSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace TDInputCaptureComponent
{
    void TDInputCaptureComponentSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<TDInputCaptureComponentSystemComponent, AZ::Component>()
                ->Version(0);

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<TDInputCaptureComponentSystemComponent>("TDInputCaptureComponent", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
					->Attribute(AZ::Edit::Attributes::Category, "TowerDefence")
                    ;
            }
        }
    }

    void TDInputCaptureComponentSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("TDInputCaptureComponentService"));
    }

    void TDInputCaptureComponentSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("TDInputCaptureComponentService"));
    }

    void TDInputCaptureComponentSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void TDInputCaptureComponentSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

	bool TDInputCaptureComponentSystemComponent::OnInputChannelEventFiltered(const AzFramework::InputChannel& channel)
	{
		const AzFramework::InputDeviceId id = channel.GetInputDevice().GetInputDeviceId();

		if (id == AzFramework::InputDeviceKeyboard::Id)
		{
			//const AzFramework::InputChannelId i = channel.GetInputChannelId();
			//if (i == AzFramework::InputDeviceKeyboard::Key::AlphanumericS)
			//{
			//	if (!!channel.GetValue())
			//	{
			//		if (!bIsButtonPressed)
			//		{
			//			TDPlayerController::TDPlayerControllerRequestBus::Broadcast(&TDPlayerController::TDPlayerControllerRequestBus::Events::BeginPlace);
			//			bIsButtonPressed = true;
			//		}
			//	}
			//	else
			//	{
			//		bIsButtonPressed = false;
			//	}
			//}
		}
		else if (id == AzFramework::InputDeviceMouse::Id)
		{
			const AzFramework::InputChannelId _i = channel.GetInputChannelId();
			if (_i == AzFramework::InputDeviceMouse::Button::Left)
			{
				if (!!channel.GetValue())
				{
					if (!bIsButtonPressed)
					{
						bIsButtonPressed = true;
						AZ_Printf("TDICSC", "%s", "Clicked");
						Projector::ProjectionSpawnerRequestsBus::Broadcast(&Projector::ProjectionSpawnerRequestsBus::Events::SpawnProjection, AZ::Vector3(.0f, .0f, .0f));
					}
					else
					{
						bIsButtonPressed = false;
					}
				}
			}
			else if (_i == AzFramework::InputDeviceMouse::SystemCursorPosition)
			{
				const AzFramework::InputChannel::PositionData2D* pos = channel.GetCustomData<AzFramework::InputChannel::PositionData2D>();
				if (!pos) return false;
				const AZ::Vector2& _p = pos->m_normalizedPosition;
				const AZ::Vector2& ssCoords = pos->ConvertToScreenSpaceCoordinates(m_viewportDimens.GetX(), m_viewportDimens.GetY());
				static const AZ::Vector2 centre = AZ::Vector2{ .5f, .5f };


				AZ::Vector2 actualCentre = centre * m_viewportDimens;

				/*AZ::Vector2 posToReport = (ssCoords - actualCentre);*/
				/*AZ::Vector2 posToReport{ (ssCoords.GetX() - actualCentre.GetX()), 1 - (ssCoords.GetY() - actualCentre.GetY()) };*/
				AZ::Vector2 posToReport{ 1- (ssCoords.GetY() - actualCentre.GetY()) , 1-(ssCoords.GetX() - actualCentre.GetX()) };

				AZ::Vector2 _l = actualCentre + posToReport;
				AZ::Vector2 _m = _l - actualCentre;

				Projector::ProjectorRequestBus::Broadcast(&Projector::ProjectorRequestBus::Events::Move, posToReport);
			}
		}

		return false;
	}

    void TDInputCaptureComponentSystemComponent::Init()
    {
    }

    void TDInputCaptureComponentSystemComponent::Activate()
    {
        TDInputCaptureComponentRequestBus::Handler::BusConnect();
		AzFramework::InputChannelEventListener::BusConnect();
    }

    void TDInputCaptureComponentSystemComponent::Deactivate()
    {
        TDInputCaptureComponentRequestBus::Handler::BusDisconnect();
    }
}
