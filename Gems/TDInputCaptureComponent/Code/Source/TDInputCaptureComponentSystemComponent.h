#pragma once

#include <AzCore/Component/Component.h>

#include <AzFramework/Input/Events/InputChannelEventListener.h>
#include <AzFramework/Input/Devices/Keyboard/InputDeviceKeyboard.h>
#include <AzFramework/Input/Devices/Mouse/InputDeviceMouse.h>
#include <AzFramework/Physics/PhysicsSystemComponentBus.h>
#include <AzFramework/Application/Application.h>

#include <Projector/ProjectorBus.h>
#include <Projector/ProjectionSpawnerBus.h>

#include <TDPlayerController/TDPlayerControllerBus.h>

#include <TDInputCaptureComponent/TDInputCaptureComponentBus.h>

namespace TDInputCaptureComponent
{
    class TDInputCaptureComponentSystemComponent
        : public AZ::Component
		, public AzFramework::InputChannelEventListener
        , protected TDInputCaptureComponentRequestBus::Handler
    {
    public:
        AZ_COMPONENT(TDInputCaptureComponentSystemComponent, "{95CD2C79-0325-44A3-A134-F3B4AEE57F02}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // TDInputCaptureComponentRequestBus interface implementation
		bool OnInputChannelEventFiltered(const AzFramework::InputChannel&) override;
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
	private:
		bool bIsButtonPressed = false;
		AZ::Vector2 m_mouseChangeAggregate = AZ::Vector2{ .5f, .5f };
		AZ::Vector2 dimens{ 1920.f, 1080.f };
		AZ::Vector2 m_viewportDimens{ 54.73f, 30.5f };
    };
}
