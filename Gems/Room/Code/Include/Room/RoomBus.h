#pragma once

#include <AzCore/EBus/EBus.h>

namespace Room
{
    class RoomRequests
        : public AZ::EBusTraits
    {
    public:
		AZ_TYPE_INFO(RoomRequests, "{F117EB86-47B9-46E9-AD26-FDFB99F1B836}");
        //////////////////////////////////////////////////////////////////////////
        // EBusTraits overrides
        static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
        static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::ById;

		using BusIdType = AZ::EntityId;
        //////////////////////////////////////////////////////////////////////////
        // Put your public methods here

		struct SGas
		{
			AZ_TYPE_INFO(SGas, "{41176F44-AF48-42AA-94C8-228CE2A5F35B}");
			AZ_CLASS_ALLOCATOR(SGas, AZ::SystemAllocator, 0);
			SGas()
				: m_gasName("")
				,m_composition(0.f) {}
			AZStd::string m_gasName;
			float m_composition;
			inline bool operator==(const SGas& rhs) const
			{
				return (this->m_gasName == rhs.m_gasName && this->m_composition == m_composition);
			}

			inline bool operator!=(const SGas& rhs) const
			{
				return (this->m_gasName != rhs.m_gasName || this->m_composition != rhs.m_composition);
			}
		};

		struct SClimate
		{
			AZ_TYPE_INFO(SClimate, "{07B68CCD-FEB1-47B9-B08A-A5256CF7808E}");
			AZ_CLASS_ALLOCATOR(SClimate, AZ::SystemAllocator, 0);
			SClimate()
				: m_temperature(0.f)
				, m_humidity(0.f) {}
			float m_temperature = 1.f;
			float m_humidity;
			inline bool operator==(const SClimate& rhs) const
			{
				return (this->m_temperature == rhs.m_temperature && this->m_humidity == rhs.m_humidity);
			}

			inline bool operator!=(const SClimate& rhs) const
			{
				return (this->m_temperature != rhs.m_temperature || this->m_humidity != rhs.m_humidity);
			}
		};

		struct SAtmo
		{
			AZ_TYPE_INFO(SAtmo, "{17DB1F20-ACFC-45E1-AD8D-95CB652860B2}");
			AZ_CLASS_ALLOCATOR(SClimate, AZ::SystemAllocator, 0);
			SAtmo()
				: m_gasses()
				, m_climate()
				, m_gravity() {}
			AZStd::vector<SGas> m_gasses;
			SClimate m_climate;
			float m_gravity = 9.81f;

			inline bool operator==(const SAtmo& rhs) const
			{
				return (this->m_gasses == rhs.m_gasses && this->m_climate == rhs.m_climate
					&& this->m_gravity == rhs.m_gravity);
			}

			inline bool operator!=(const SAtmo& rhs) const
			{
				return (this->m_gasses != rhs.m_gasses || this->m_climate != rhs.m_climate
					|| this->m_gravity != rhs.m_gravity);
			}
		};

		virtual SAtmo GetAtmosphere() = 0;
    };
    using RoomRequestBus = AZ::EBus<RoomRequests>;
} // namespace Room
