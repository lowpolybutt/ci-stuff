#pragma once

#include <AzCore/EBus/EBus.h>
#include <Room/RoomBus.h>

namespace Room
{
	class RoomUiRequests
		: public AZ::EBusTraits
	{
	public:
		static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
		static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::ById;

		using BusIdType = AZ::EntityId;

		virtual void UpdateRoom(RoomRequests::SAtmo) = 0;
	};

	using RoomUiRequestBus = AZ::EBus<RoomUiRequests>;
}