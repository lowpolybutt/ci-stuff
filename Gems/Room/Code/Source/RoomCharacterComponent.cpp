#include "RoomCharacterComponent.h"

namespace Room
{
	void RoomCharacterComponent::Reflect(AZ::ReflectContext* c)
	{
		if (AZ::SerializeContext* s = azrtti_cast<AZ::SerializeContext*>(c))
		{
			s->Class<RoomCharacterComponent, AZ::Component>()
				->Version(1)
				->Field("Health", &RoomCharacterComponent::m_health)
				->Field("MinTemp", &RoomCharacterComponent::m_minTemp)
				->Field("MaxTemp", &RoomCharacterComponent::m_maxTemp)
				->Field("InternalTemp", &RoomCharacterComponent::m_internalTemp)
				->Field("HeatEmergency", &RoomCharacterComponent::m_heatEmergency)
				->Field("ColdEmergency", &RoomCharacterComponent::m_coldEmergency)
				->Field("TempDecayRate", &RoomCharacterComponent::m_tempDecayRate)
				;

			if (AZ::EditContext* e = s->GetEditContext())
			{
				e->Class<RoomCharacterComponent>("Room Character Component", "The component to be added to have a character interact with the room")
					->ClassElement(AZ::Edit::ClassElements::EditorData, "")
					->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
					->Attribute(AZ::Edit::Attributes::AutoExpand, true)
					->DataElement(AZ::Edit::UIHandlers::Default, &RoomCharacterComponent::m_health, "Player Health",
						"The base health of the player")
					->DataElement(AZ::Edit::UIHandlers::Default, &RoomCharacterComponent::m_minTemp, "Min Temp",
						"The minimum temperature before internal temp decay begins")
					->DataElement(AZ::Edit::UIHandlers::Default, &RoomCharacterComponent::m_maxTemp, "Max Temp",
						"The maximum temperature before internal temp 'upcay' begins")
					->DataElement(AZ::Edit::UIHandlers::Default, &RoomCharacterComponent::m_internalTemp, "Internal Temp",
						"The internal temperature of the player")
					->DataElement(AZ::Edit::UIHandlers::Default, &RoomCharacterComponent::m_heatEmergency, "Heat Emergency Temp",
						"The temp at which a medical emergency for hyperthermia is declared")
					->DataElement(AZ::Edit::UIHandlers::Default, &RoomCharacterComponent::m_coldEmergency, "Cold Emergency Temp",
						"The temp at which a medical emergency for hypothermia is declared")
					->DataElement(AZ::Edit::UIHandlers::Default, &RoomCharacterComponent::m_tempDecayRate, "Temp Decay Rate",
						"The rate of change in internal temperature per tick")
					;
			}
		}
	}

	void RoomCharacterComponent::Init()
	{}

	void RoomCharacterComponent::Activate()
	{
		//AZStd::thread_desc d;
		//AZStd::function<void()> func = AZStd::bind(&RoomCharacterComponent::FindRoom);
		//AZStd::thread(func, &d);
		AZ::TickBus::Handler::BusConnect();
	}

	void RoomCharacterComponent::Deactivate()
	{
		AZ_Printf("RCC", "Detaching");
		if (m_decayThread.joinable()) m_decayThread.detach();
		m_shouldChangeTemp = false;
		m_resetLock = true;
		AZ::TickBus::Handler::BusDisconnect();
	}

	void RoomCharacterComponent::OnTick(float, AZ::ScriptTimePoint)
	{
		FindRoom();
	}

	void RoomCharacterComponent::FindRoom()
	{
		AZ::Vector3 org;
		AZ::TransformBus::EventResult(org, GetEntityId(), &AZ::TransformBus::Events::GetWorldTranslation);
		AzFramework::PhysicsSystemRequests::RayCastConfiguration conf;
		conf.m_origin = org;
		conf.m_direction = AZ::Vector3::CreateAxisZ();
		conf.m_maxHits = 10;
		conf.m_maxDistance = 10.f;
		conf.m_ignoreEntityIds = AZStd::vector<AZ::EntityId>{ GetEntityId() };

		const AzFramework::PhysicsSystemRequests::RayCastHit* hit;
		AzFramework::PhysicsSystemRequests::RayCastResult res;
		AzFramework::PhysicsSystemRequestBus::BroadcastResult(res, &AzFramework::PhysicsSystemRequestBus::Events::RayCast, conf);
		if (res.GetHitCount() > 0)
		{
			hit = res.GetHit(0);
			AZ::EntityId hitEntityId = hit->m_entityId;
			AZ::Entity* e = nullptr;
			AZ::ComponentApplicationBus::BroadcastResult(e, &AZ::ComponentApplicationBus::Events::FindEntity, hitEntityId);

			if (e != nullptr)
			{
				AZ::Entity::ComponentArrayType arr = e->GetComponents();

				for (size_t i = 0; i < arr.size(); i++)
				{
					if (Room::RoomSystemComponent* rc = static_cast<Room::RoomSystemComponent*>(arr[i]))
					{
						Room::RoomRequests::SAtmo a;
						Room::RoomRequestBus::EventResult(a, hitEntityId, &Room::RoomRequestBus::Events::GetAtmosphere);
						Room::RoomUiRequestBus::Broadcast(&Room::RoomUiRequestBus::Events::UpdateRoom, a);

						if (s != a)
						{
							m_shouldChangeTemp = false;
							if (m_decayThread.joinable()) m_decayThread.detach();

							if (a.m_climate.m_temperature > m_maxTemp || a.m_climate.m_temperature < m_minTemp)
							{
								m_shouldChangeTemp = true;
								AZ_Printf("RCC", "It's climate time");
								AZStd::thread_desc d;
								AZStd::function<void()> f;
								if (a.m_climate.m_temperature > m_maxTemp)
								{
									d.m_name = "UpcayTempThr";
									f = AZStd::bind(&RoomCharacterComponent::UpcayInternalTemperature, this);
								}
								else
								{
									d.m_name = "DecayTempThr";
									f = AZStd::bind(&RoomCharacterComponent::DecayInternalTemperature, this);
								}
								m_decayThread = AZStd::thread(f, &d);
							}
							else
							{
								AZ_Printf("RCC", "MMMMM COSY");
							}

							if (a.m_climate.m_temperature < m_maxTemp || a.m_climate.m_temperature > m_minTemp)
							{
								AZStd::thread_desc d;
								AZStd::function<void()> f;
								f = AZStd::bind(&RoomCharacterComponent::ResetTemperature, this);
								m_decayThread = AZStd::thread(f, &d);
							}
							s = a;
						}
					}
				}
			}
		}
	}

	void RoomCharacterComponent::UpcayInternalTemperature()
	{
		do
		{
			m_resetLock = true;
			m_internalTemp += m_tempDecayRate;
			AZ_Printf("RCC", "Internal temp: %f", (float)m_internalTemp);
			AZStd::this_thread::sleep_for(AZStd::chrono::seconds(1));
		}
		// Update to have a death temp
		while (m_shouldChangeTemp);
		if (m_decayThread.joinable()) m_decayThread.detach();
		m_resetLock = false;
	}

	void RoomCharacterComponent::DecayInternalTemperature()
	{
		do
		{
			m_resetLock = true;
			m_internalTemp -= m_tempDecayRate;
			AZ_Printf("RCC", "Internal temp: %f", (float)m_internalTemp);
			AZStd::this_thread::sleep_for(AZStd::chrono::seconds(1));
		}
		while (m_shouldChangeTemp);
		if (m_decayThread.joinable()) m_decayThread.detach();
		m_resetLock = false;
	}

	void RoomCharacterComponent::ResetTemperature()
	{
		do
		{
			if (m_internalTemp > m_baseInternalTemp)
			{
				m_internalTemp -= m_tempDecayRate;
			}
			else
			{
				m_internalTemp += m_tempDecayRate;
			}
			AZ_Printf("RCC", "Resetting temp: %f", (float)m_internalTemp);
			AZStd::this_thread::sleep_for(AZStd::chrono::seconds(1));
		}
		while (!m_resetLock);
		if (m_decayThread.joinable()) m_decayThread.detach();
	}
}
