#pragma once

#include <AzCore/Component/Component.h>

#include <Room/RoomBus.h>

namespace Room
{
    class RoomSystemComponent
        : public AZ::Component
        , protected RoomRequestBus::Handler
    {
    public:
        AZ_COMPONENT(RoomSystemComponent, "{FA53BE8B-2635-485C-9867-5BE465972C29}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

		SAtmo m_atmosphere;

    protected:
        ////////////////////////////////////////////////////////////////////////
        // RoomRequestBus interface implementation
		float m_float;
		SGas ebug;
		AZStd::vector<SGas> m_atmosGasses;
		SAtmo GetAtmosphere() override;
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
