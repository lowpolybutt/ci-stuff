
#include <RoomSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace Room
{
    void RoomSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
			/*
			* SERIALIZES THE STRUCTS
			*/
			serialize->Class<SGas>()
				->Version(0)
				->Field("GasName", &SGas::m_gasName)
				->Field("GasComp", &SGas::m_composition)
				;
				
			if (AZ::EditContext* ec = serialize->GetEditContext())
			{
				ec->Class<SGas>("Gas", "Edit the gasses")
					->DataElement(AZ::Edit::UIHandlers::Default, &SGas::m_gasName, "Gas name", "Name of gas")
					->DataElement(AZ::Edit::UIHandlers::Default, &SGas::m_composition, "Gas comp", "Percentage of the atmosphere made up by this gas")
					;
			}


			serialize->Class<SClimate>()
				->Version(0)
				->Field("Temp", &SClimate::m_temperature)
				->Field("Humidity", &SClimate::m_humidity)
				;

			if (AZ::EditContext* ec = serialize->GetEditContext())
			{
				ec->Class<SClimate>("Climate", "Edit the climate for the room")
					->DataElement(AZ::Edit::UIHandlers::Default, &SClimate::m_temperature, "Temp", "The ambient temperature in the room")
					// TODO add apparent "feels like" temperature
					->DataElement(AZ::Edit::UIHandlers::Default, &SClimate::m_humidity, "Humidity", "The humidity in the room (absolute)")
					;
			}

			serialize->Class<SAtmo>()
				->Version(1)
				->Field("GasComp", &SAtmo::m_gasses)
				->Field("Climate", &SAtmo::m_climate)
				->Field("Gravity", &SAtmo::m_gravity)
				;

			if (AZ::EditContext* ec = serialize->GetEditContext())
			{
				ec->Class<SAtmo>("Atmo", "Edit the overall atmosphere for the room")
					->DataElement(AZ::Edit::UIHandlers::Default, &SAtmo::m_gasses, "Gasses", "The gasses in the room")
					->DataElement(AZ::Edit::UIHandlers::Default, &SAtmo::m_climate, "Climate", "The climate in the room")
					->DataElement(AZ::Edit::UIHandlers::Default, &SAtmo::m_gravity, "Gravity", "The gravity in the room")
					;
			}

			/*
			* SERIALIZES THE ROOT COMPONENT
			* This will not serialize other serializable components
			*/
            serialize->Class<RoomSystemComponent, AZ::Component>()
                ->Version(2)
				->Field("Atmosphere", &RoomSystemComponent::m_atmosphere)
				;

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<RoomSystemComponent>("Room", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
					->DataElement(AZ::Edit::UIHandlers::Default, &RoomSystemComponent::m_atmosphere, "Atmosphere", "The atmosphere")
                    ;
            }
        }
    }

    void RoomSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("RoomService"));
    }

    void RoomSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("RoomService"));
    }

    void RoomSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void RoomSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

	Room::RoomRequests::SAtmo RoomSystemComponent::GetAtmosphere()
	{
		return m_atmosphere;
	}

	void RoomSystemComponent::Init()
    {
    }

    void RoomSystemComponent::Activate()
    {
        RoomRequestBus::Handler::BusConnect(GetEntityId());
    }

    void RoomSystemComponent::Deactivate()
    {
        RoomRequestBus::Handler::BusDisconnect();
    }
}
