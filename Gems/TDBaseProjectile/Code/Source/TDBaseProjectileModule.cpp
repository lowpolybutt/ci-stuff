
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <TDBaseProjectileSystemComponent.h>

namespace TDBaseProjectile
{
    class TDBaseProjectileModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(TDBaseProjectileModule, "{8D4D5847-3FFC-466E-B1C5-38A1470FF956}", AZ::Module);
        AZ_CLASS_ALLOCATOR(TDBaseProjectileModule, AZ::SystemAllocator, 0);

        TDBaseProjectileModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                TDBaseProjectileSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<TDBaseProjectileSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(TDBaseProjectile_e2e3ddc8e64946bb93747f40bedeb633, TDBaseProjectile::TDBaseProjectileModule)
