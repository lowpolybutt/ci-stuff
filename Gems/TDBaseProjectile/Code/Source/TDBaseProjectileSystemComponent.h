#pragma once

#include <AzCore/Component/Component.h>

#include <TDBaseProjectile/TDBaseProjectileBus.h>

namespace TDBaseProjectile
{
    class TDBaseProjectileSystemComponent
        : public AZ::Component
        , protected TDBaseProjectileRequestBus::Handler
    {
    public:
        AZ_COMPONENT(TDBaseProjectileSystemComponent, "{E1A145DC-AF16-4BE2-BDD4-8939D1BF075A}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // TDBaseProjectileRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
