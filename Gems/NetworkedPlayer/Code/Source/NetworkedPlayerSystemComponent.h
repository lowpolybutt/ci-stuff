#pragma once

#include <AzCore/Component/Component.h>

#include <NetworkedPlayer/NetworkedPlayerBus.h>

namespace NetworkedPlayer
{
    class NetworkedPlayerSystemComponent
        : public AZ::Component
        , protected NetworkedPlayerRequestBus::Handler
    {
    public:
        AZ_COMPONENT(NetworkedPlayerSystemComponent, "{4AE54BCC-8AA2-4769-832E-E3D3745992CF}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // NetworkedPlayerRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
