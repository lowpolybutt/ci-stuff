
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <BaseBallisticSystemComponent.h>

namespace BaseBallistic
{
    class BaseBallisticModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(BaseBallisticModule, "{71AE8A73-6E59-4636-83BC-1CBF6D2E8A36}", AZ::Module);
        AZ_CLASS_ALLOCATOR(BaseBallisticModule, AZ::SystemAllocator, 0);

        BaseBallisticModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                BaseBallisticSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<BaseBallisticSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(BaseBallistic_c4fb840561c441e894bd290a4eb83faa, BaseBallistic::BaseBallisticModule)
