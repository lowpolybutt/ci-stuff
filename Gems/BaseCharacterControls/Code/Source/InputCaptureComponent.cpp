#include "InputCaptureComponent.h"

#include <AzFramework/Input/Devices/Keyboard/InputDeviceKeyboard.h>
#include <AzFramework/Input/Devices/Mouse/InputDeviceMouse.h>

#include <Integration/AnimGraphComponentBus.h>

#include <AzCore/Serialization/EditContext.h>

using namespace BaseCharacterControls;

void InputCaptureComponent::Activate()
{
	AzFramework::InputChannelEventListener::BusConnect();
}

void InputCaptureComponent::Deactivate()
{
	AzFramework::InputChannelEventListener::BusDisconnect();
}

void InputCaptureComponent::Reflect(AZ::ReflectContext* ctx)
{
	AZ::SerializeContext* s = azrtti_cast<AZ::SerializeContext*>(ctx);
	if (!s) return;
	s->Class<InputCaptureComponent, Component>()
		->Version(0)
		;

	AZ::EditContext* e = s->GetEditContext();
	if (!e) return;
	e->Class<InputCaptureComponent>("Character input capture", "[Captures input]")
		->ClassElement(AZ::Edit::ClassElements::EditorData, "")
		->Attribute(AZ::Edit::Attributes::Category, "Base Controls")
		->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
		;
}

bool InputCaptureComponent::OnInputChannelEventFiltered(
	const AzFramework::InputChannel& channel
)
{
	AzFramework::InputDeviceId devId = channel.GetInputDevice().GetInputDeviceId();

	if (devId == AzFramework::InputDeviceKeyboard::Id)
	{
		return OnKeyboardEvent(channel);
	}
	return true;
}

bool InputCaptureComponent::OnKeyboardEvent(const AzFramework::InputChannel& channel)
{
	const AzFramework::InputChannelId type = channel.GetInputChannelId();

	if (type == AzFramework::InputDeviceKeyboard::Key::AlphanumericW)
	{
		MoveForward(!!channel.GetValue());
	}
	else if (type == AzFramework::InputDeviceKeyboard::Key::AlphanumericS)
	{
		MoveBackward(!!channel.GetValue());
	}
	else if (type == AzFramework::InputDeviceKeyboard::Key::AlphanumericD)
	{
		StrafeLeft(!!channel.GetValue());
	}
	else if (type == AzFramework::InputDeviceKeyboard::Key::AlphanumericA)
	{
		StrafeRight(!!channel.GetValue());
	}
	return true;
}

void InputCaptureComponent::MoveForward(bool bIsPressed)
{
	if (bIsPressed)
	{
		//SetEmotionParam("speed", 1.f);
		SetEmotionParam("Speed", 1.f);
	}
	else
	{
		/*SetEmotionParam("speed", .0f);*/
		SetEmotionParam("Speed", 0.f);
	}
}

void InputCaptureComponent::MoveBackward(bool bIsPressed)
{
	if (bIsPressed)
	{
		SetEmotionParam("speed", -1.f);
	}
	else
	{
		SetEmotionParam("speed", .0f);
	}
}

void InputCaptureComponent::StrafeLeft(bool bIsPressed)
{
	if (bIsPressed)
	{
		SetEmotionParam("x", -1.f);
	}
	else
	{
		SetEmotionParam("x", .0f);
	}
}

void InputCaptureComponent::StrafeRight(bool bIsPressed)
{
	if (bIsPressed)
	{
		SetEmotionParam("x", 1.f);
	}
	else
	{
		SetEmotionParam("x", .0f);
	}
}

void InputCaptureComponent::SetEmotionParam(const char* param, float val)
{
	EMotionFX::Integration::AnimGraphComponentRequestBus::Event(
		GetEntityId(),
		&EMotionFX::Integration::AnimGraphComponentRequestBus::Events
		::SetNamedParameterFloat,
		param, val
	);
}