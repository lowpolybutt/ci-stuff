#include "ServerAuthHeroComponent.h"
#include <AzCore/Serialization/EditContext.h>
#include <AzFramework/Network/NetworkContext.h>
#include <GridMate/Replica/ReplicaChunk.h>
#include <AzFramework/Network/NetBindingHandlerBus.h>
#include <GridMate/Replica/RemoteProcedureCall.h>
#include <GridMate/Replica/ReplicaFunctions.h>
#include <BaseCharacterControls/LocalClientBus.h>

using namespace BaseCharacterControls;

class ServerAuthHeroComponent::HeroChunk
	: public GridMate::ReplicaChunkBase
{
public:
	AZ_CLASS_ALLOCATOR(HeroChunk, AZ::SystemAllocator, 0);

	HeroChunk()
		: m_owningPlayer("Owning Player")
	{
	}

	static const char* GetChunkName()
	{
		return "ServerAuthHeroComponent::HeroChunk";
	}

	bool IsReplicaMigratable() override { return true; }

	GridMate::DataSet<GridMate::MemberIDCompact> m_owningPlayer;
};

void ServerAuthHeroComponent::Reflect(AZ::ReflectContext* ctx)
{
	AZ::SerializeContext* s = azrtti_cast<AZ::SerializeContext*>(ctx);

	if (!s) return;
	s->Class<ServerAuthHeroComponent, AzFramework::NetBindable, Component>()
		->Version(0)
		;

	AZ::EditContext* e = s->GetEditContext();
	if (!e) return;
	e->Class<ServerAuthHeroComponent>("Server Authoritative Hero Component",
		"Server Authoritative")
		->ClassElement(AZ::Edit::ClassElements::EditorData, "")
		->Attribute(AZ::Edit::Attributes::Category, "Base Hero")
		->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
		;

	AzFramework::NetworkContext* n = azrtti_cast<AzFramework::NetworkContext*>(ctx);

	if (!n) return;
	n->Class<ServerAuthHeroComponent>()
		->Chunk<HeroChunk>()
		;

	auto& descTable = GridMate::ReplicaChunkDescriptorTable::Get();
	if (!descTable.FindReplicaChunkDescriptor(
		GridMate::ReplicaChunkClassId(HeroChunk::GetChunkName())
	))
	{
		descTable.RegisterChunkType<HeroChunk>();
	}
}

GridMate::ReplicaChunkPtr BaseCharacterControls::ServerAuthHeroComponent::GetNetworkBinding()
{
	return GridMate::ReplicaChunkPtr();
}

void BaseCharacterControls::ServerAuthHeroComponent::SetNetworkBinding(GridMate::ReplicaChunkPtr)
{
}

void BaseCharacterControls::ServerAuthHeroComponent::UnbindFromNetwork()
{
}

void BaseCharacterControls::ServerAuthHeroComponent::Activate()
{
}

void BaseCharacterControls::ServerAuthHeroComponent::Deactivate()
{
}

void ServerAuthHeroComponent::SetAssociatedPlayerId(GridMate::MemberIDCompact cmpct)
{
	if (HeroChunk* c = static_cast<HeroChunk*>(m_heroChunk.get()))
		c->m_owningPlayer.Set(cmpct);
}

