
#include <BaseCharacterControlsSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace BaseCharacterControls
{
    void BaseCharacterControlsSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<BaseCharacterControlsSystemComponent, AZ::Component>()
                ->Version(0);

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<BaseCharacterControlsSystemComponent>("BaseCharacterControls", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("System"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ;
            }
        }
    }

    void BaseCharacterControlsSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("BaseCharacterControlsService"));
    }

    void BaseCharacterControlsSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("BaseCharacterControlsService"));
    }

    void BaseCharacterControlsSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void BaseCharacterControlsSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

    void BaseCharacterControlsSystemComponent::Init()
    {
		AZ_Printf("BCCC", "Initialized Controls Component");
    }

    void BaseCharacterControlsSystemComponent::Activate()
    {
        BaseCharacterControlsRequestBus::Handler::BusConnect();
    }

    void BaseCharacterControlsSystemComponent::Deactivate()
    {
        BaseCharacterControlsRequestBus::Handler::BusDisconnect();
    }

	void BaseCharacterControlsSystemComponent::MoveForward(ActionState st)
	{

	}

	void BaseCharacterControlsSystemComponent::MoveBackward(ActionState st)
	{
		AZ_UNUSED(st);
	}

	void BaseCharacterControlsSystemComponent::MoveLeft(ActionState st)
	{
		AZ_UNUSED(st);
	}

	void BaseCharacterControlsSystemComponent::MoveRight(ActionState st)
	{
		AZ_UNUSED(st);
	}

	void BaseCharacterControlsSystemComponent::LookVertical(float amt)
	{
		AZ_UNUSED(amt);
	}

	void BaseCharacterControlsSystemComponent::LookHorizontal(float amt)
	{
		AZ_UNUSED(amt);
	}
}
