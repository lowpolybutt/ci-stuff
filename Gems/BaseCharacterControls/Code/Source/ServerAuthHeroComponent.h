#pragma once

#include <AzCore/Component/Component.h>
#include <AzCore/Component/TickBus.h>
#include <GridMate/Replica/ReplicaCommon.h>
#include <AzFramework/Network/NetBindable.h>
#include <BaseCharacterControls/ServerActorBus.h>

namespace BaseCharacterControls
{
	class ServerAuthHeroComponent
		: public AZ::Component
		, public AzFramework::NetBindable
		, public ServerActorBus::Handler
	{
	public:
		AZ_COMPONENT(ServerAuthHeroComponent, "{68D0EBF9-2D46-4F80-8343-69358B09ED3F}",
			NetBindable);

		static void Reflect(AZ::ReflectContext*);

		GridMate::ReplicaChunkPtr GetNetworkBinding() override;
		void SetNetworkBinding(GridMate::ReplicaChunkPtr) override;
		void UnbindFromNetwork() override;

	protected:
		void Activate() override;
		void Deactivate() override;


		void SetAssociatedPlayerId(GridMate::MemberIDCompact) override;

	private:
		class HeroChunk;
		GridMate::ReplicaChunkPtr m_heroChunk;

		void BroadcastNewPlayer();

#if defined(DEDICATED_SERVER)
		constexpr bool IsDedicated() const { return true; }
#else 
		constexpr bool IsDedicated() const { return false; }
#endif
	};
}