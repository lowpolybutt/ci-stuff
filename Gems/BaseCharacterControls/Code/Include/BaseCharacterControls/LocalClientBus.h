#pragma once
#include <AzCore/EBus/EBus.h>
#include <GridMate/Session/Session.h>
#include <AzCore/Component/EntityId.h>

namespace BaseCharacterControls
{
	class LocalClientInterface
		: public AZ::EBusTraits
	{
	public:
		virtual ~LocalClientInterface() = default;

		static const AZ::EBusHandlerPolicy HandlerPolicy
			= AZ::EBusHandlerPolicy::Single;

		static const AZ::EBusAddressPolicy AddressPolicy
			= AZ::EBusAddressPolicy::Single;

		virtual void AttachToBody(
			GridMate::MemberIDCompact,
			const AZ::EntityId&
		) = 0;
	};
	using LocalClientBus = AZ::EBus<LocalClientInterface>;
}