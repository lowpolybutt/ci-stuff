#pragma once

#include <AzCore/EBus/EBus.h>
#include <AzCore/Component/ComponentBus.h>
#include <GridMate/Session/Session.h>

namespace BaseCharacterControls
{
	class ServerActorInterface
		: public AZ::ComponentBus
	{
	public:
		virtual ~ServerActorInterface() = default;

		virtual void SetAssociatedPlayerId(GridMate::MemberIDCompact) = 0;
	};

	using ServerActorBus = AZ::EBus<ServerActorInterface>;
}