#pragma once

#include <AzCore/EBus/EBus.h>

namespace BaseCharacterControls
{
	enum class ActionState
	{
		Started,
		Stopped
	};

    class BaseCharacterControlsRequests
        : public AZ::EBusTraits
    {
    public:
        //////////////////////////////////////////////////////////////////////////
        // EBusTraits overrides
        static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
        static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::Single;
        //////////////////////////////////////////////////////////////////////////
        // Put your public methods here
		virtual void MoveForward(ActionState) {}
		virtual void MoveBackward(ActionState) {}
		virtual void MoveLeft(ActionState) {}
		virtual void MoveRight(ActionState) {}
		virtual void LookVertical(float) {}
		virtual void LookHorizontal(float) {}
    };
    using BaseCharacterControlsRequestBus = AZ::EBus<BaseCharacterControlsRequests>;
} // namespace BaseCharacterControls
