
#include <RoomSystemSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace RoomSystem
{
    void RoomSystemSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<RoomSystemSystemComponent, AZ::Component>()
                ->Version(0)
				->Field("AtmoComp_oxy", &RoomSystemSystemComponent::m_oxyComp)
				->Field("AtmoComp_nitro", &RoomSystemSystemComponent::m_nitroComp)
				->Field("AtmoComp_carbonDiox", &RoomSystemSystemComponent::m_carbonDioxComp)
				;

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<RoomSystemSystemComponent>("RoomSystem", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
						->Attribute(AZ::Edit::Attributes::Category, "TowerDefence")
					->DataElement(AZ::Edit::UIHandlers::Default, &RoomSystemSystemComponent::m_oxyComp, "Oxygen Composition", "The oxygen content of the room")
						->Attribute(AZ::Edit::Attributes::ChangeNotify, 
							&RoomSystemSystemComponent::UpdateClimateOxy)
                    ;
            }
        }
    }

    void RoomSystemSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("RoomSystemService"));
    }

    void RoomSystemSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("RoomSystemService"));
    }

    void RoomSystemSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void RoomSystemSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

    void RoomSystemSystemComponent::Init()
    {
    }

    void RoomSystemSystemComponent::Activate()
    {
        RoomSystemRequestBus::Handler::BusConnect();
    }

    void RoomSystemSystemComponent::Deactivate()
    {
        RoomSystemRequestBus::Handler::BusDisconnect();
    }

	void RoomSystemSystemComponent::UpdateClimateOxy(float oxy)
	{
		m_atmoComp.m_oxyComp.m_atmosComp = oxy;
		m_atmoComp.m_oxyComp.m_gasName = "Oxygen";
	}
}
