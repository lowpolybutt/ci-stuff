
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <RoomSystemSystemComponent.h>

namespace RoomSystem
{
    class RoomSystemModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(RoomSystemModule, "{B9986571-78D7-490E-A1E2-3F206FB5C55F}", AZ::Module);
        AZ_CLASS_ALLOCATOR(RoomSystemModule, AZ::SystemAllocator, 0);

        RoomSystemModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                RoomSystemSystemComponent::CreateDescriptor(),
            });
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(RoomSystem_20cf72136d364610a499cf121defc090, RoomSystem::RoomSystemModule)
