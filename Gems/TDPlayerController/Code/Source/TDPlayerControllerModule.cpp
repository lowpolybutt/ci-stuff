
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <TDPlayerControllerSystemComponent.h>

namespace TDPlayerController
{
    class TDPlayerControllerModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(TDPlayerControllerModule, "{8D8A4612-B0DA-4FF8-9A41-E3A42792A7DE}", AZ::Module);
        AZ_CLASS_ALLOCATOR(TDPlayerControllerModule, AZ::SystemAllocator, 0);

        TDPlayerControllerModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                TDPlayerControllerSystemComponent::CreateDescriptor(),
            });
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(TDPlayerController_37a2fd4b2b074419a47f1aefab5c7b45, TDPlayerController::TDPlayerControllerModule)
