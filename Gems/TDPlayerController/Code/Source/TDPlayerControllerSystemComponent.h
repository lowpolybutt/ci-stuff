#pragma once

#include <AzCore/Component/Component.h>
#include <AzCore/Math/Vector3.h>

#include <AzFramework/Input/Buses/Requests/InputSystemCursorRequestBus.h>

#include <TDPlayerController/TDPlayerControllerBus.h>

namespace TDPlayerController
{
    class TDPlayerControllerSystemComponent
        : public AZ::Component
        , protected TDPlayerControllerRequestBus::Handler
    {
    public:
        AZ_COMPONENT(TDPlayerControllerSystemComponent, "{875D7F0C-B3C7-4F3C-A909-2C80E32EF1A6}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // TDPlayerControllerRequestBus interface implementation
		void BeginPlace() override;
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
		void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
	private:
		float w = 1920.f;
		float h = 1080.f;
    };
}
