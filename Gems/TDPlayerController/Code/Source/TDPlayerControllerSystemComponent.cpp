
#include <TDPlayerControllerSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace TDPlayerController
{
    void TDPlayerControllerSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<TDPlayerControllerSystemComponent, AZ::Component>()
                ->Version(0);

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<TDPlayerControllerSystemComponent>("TDPlayerController", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
					->Attribute(AZ::Edit::Attributes::Category, "TowerDefence")
                    ;
            }
        }
    }

    void TDPlayerControllerSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("TDPlayerControllerService"));
    }

    void TDPlayerControllerSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("TDPlayerControllerService"));
    }

    void TDPlayerControllerSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void TDPlayerControllerSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

	void TDPlayerControllerSystemComponent::BeginPlace()
	{
		AZ_Printf("TDPSC", "%s", "hi :)");
		AZ::Vector2 mousePos;
		AzFramework::InputSystemCursorRequestBus::BroadcastResult(mousePos, &AzFramework::InputSystemCursorRequestBus::Events::GetSystemCursorPositionNormalized);
		AZ::Vector3 v{ (mousePos.GetX() * w), (mousePos.GetY() * h), 34.f };
		AZ_Printf("TDPSC", "%s: %f %f. %s %f %f %f", "Mouse is at", (float)mousePos.GetX(), (float)mousePos.GetY(),
			"Click at", (float)v.GetX(), (float)v.GetY(), (float)v.GetZ());
	}


	void TDPlayerControllerSystemComponent::Init()
	{}

	void TDPlayerControllerSystemComponent::Activate()
    {
        TDPlayerControllerRequestBus::Handler::BusConnect();
    }

    void TDPlayerControllerSystemComponent::Deactivate()
    {
        TDPlayerControllerRequestBus::Handler::BusDisconnect();
    }
}
