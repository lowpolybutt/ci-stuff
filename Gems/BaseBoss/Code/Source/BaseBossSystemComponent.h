#pragma once

#include <AzCore/Component/Component.h>

#include <BaseBoss/BaseBossBus.h>

namespace BaseBoss
{
    class BaseBossSystemComponent
        : public AZ::Component
        , protected BaseBossRequestBus::Handler
    {
    public:
        AZ_COMPONENT(BaseBossSystemComponent, "{811BDE60-E92B-4F03-AD07-72F031C89BB7}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // BaseBossRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
