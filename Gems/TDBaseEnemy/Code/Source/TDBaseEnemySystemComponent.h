#pragma once

#include <AzCore/Component/Component.h>

#include <TDBaseEnemy/TDBaseEnemyBus.h>

namespace TDBaseEnemy
{
    class TDBaseEnemySystemComponent
        : public AZ::Component
        , protected TDBaseEnemyRequestBus::Handler
    {
    public:
        AZ_COMPONENT(TDBaseEnemySystemComponent, "{25733432-DDA5-4FC8-AB43-5750062D10D4}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // TDBaseEnemyRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
