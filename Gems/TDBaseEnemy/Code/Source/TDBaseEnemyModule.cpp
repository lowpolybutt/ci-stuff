
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <TDBaseEnemySystemComponent.h>

namespace TDBaseEnemy
{
    class TDBaseEnemyModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(TDBaseEnemyModule, "{410DCDD4-EB59-44F7-AD05-AA3C053A044D}", AZ::Module);
        AZ_CLASS_ALLOCATOR(TDBaseEnemyModule, AZ::SystemAllocator, 0);

        TDBaseEnemyModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                TDBaseEnemySystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<TDBaseEnemySystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(TDBaseEnemy_823d91c57a4c476aa222180b81a19f46, TDBaseEnemy::TDBaseEnemyModule)
