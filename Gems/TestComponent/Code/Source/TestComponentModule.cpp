
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <TestComponentSystemComponent.h>

namespace TestComponent
{
    class TestComponentModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(TestComponentModule, "{F0295EE4-03CF-4DAD-84E5-ACC2298274E3}", AZ::Module);
        /*AZ_CLASS_ALLOCATOR(TestComponentModule, AZ::SystemAllocator, 0);*/

        TestComponentModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                TestComponentSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        /*AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<TestComponentSystemComponent>(),
            };
        }*/
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(TestComponent_8b3a46152f93420fb70ece5fc2368672, TestComponent::TestComponentModule)
