#pragma once

#include <AzCore/Component/Component.h>

#include <TestComponent/TestComponentBus.h>

namespace TestComponent
{
    class TestComponentSystemComponent
        : public AZ::Component
        , protected TestComponentRequestBus::Handler
    {
    public:
        AZ_COMPONENT(TestComponentSystemComponent, "{4D6643DE-FE20-466F-83D7-A04284D68502}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

		AZStd::string m_someProperty;

    protected:
        ////////////////////////////////////////////////////////////////////////
        // TestComponentRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////

	private:
		AZStd::string m_privateProperty;
    };
}
