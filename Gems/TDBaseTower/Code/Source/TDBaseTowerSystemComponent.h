#pragma once

#include <AzCore/Component/Component.h>

#include <TDBaseTower/TDBaseTowerBus.h>

namespace TDBaseTower
{
    class TDBaseTowerSystemComponent
        : public AZ::Component
        , protected TDBaseTowerRequestBus::Handler
    {
    public:
        AZ_COMPONENT(TDBaseTowerSystemComponent, "{AC0C4F85-BF37-4C7F-B18B-C94927F007F0}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // TDBaseTowerRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
