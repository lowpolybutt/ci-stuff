
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <TDBaseTowerSystemComponent.h>

namespace TDBaseTower
{
    class TDBaseTowerModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(TDBaseTowerModule, "{E0D2E062-0C2A-4BE1-A298-92BF6A8594D6}", AZ::Module);
        AZ_CLASS_ALLOCATOR(TDBaseTowerModule, AZ::SystemAllocator, 0);

        TDBaseTowerModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                TDBaseTowerSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<TDBaseTowerSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(TDBaseTower_ead9412db33746638fbac416d5fb99a4, TDBaseTower::TDBaseTowerModule)
