#pragma once

#include <AzCore/Component/Component.h>

#include <Timer/TimerBus.h>

namespace Timer
{
    class TimerSystemComponent
        : public AZ::Component
        , protected TimerRequestBus::Handler
    {
    public:
        AZ_COMPONENT(TimerSystemComponent, "{D77681AC-A657-48B5-9E00-E360D4C5AB77}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // TimerRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
