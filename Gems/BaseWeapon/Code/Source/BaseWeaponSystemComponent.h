#pragma once

#include <AzCore/Component/Component.h>

#include <BaseWeapon/BaseWeaponBus.h>

namespace BaseWeapon
{
    class BaseWeaponSystemComponent
        : public AZ::Component
        , protected BaseWeaponRequestBus::Handler
    {
    public:
        AZ_COMPONENT(BaseWeaponSystemComponent, "{8D97440C-C7D9-4B1F-A295-E0191DE9CB86}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // BaseWeaponRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
