
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <BaseWeaponSystemComponent.h>

namespace BaseWeapon
{
    class BaseWeaponModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(BaseWeaponModule, "{3255B827-4023-4AF4-9C5E-BA0F1350863B}", AZ::Module);
        AZ_CLASS_ALLOCATOR(BaseWeaponModule, AZ::SystemAllocator, 0);

        BaseWeaponModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                BaseWeaponSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<BaseWeaponSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(BaseWeapon_9b2b150d67ad4ca8a649d5013c3d5e2d, BaseWeapon::BaseWeaponModule)
