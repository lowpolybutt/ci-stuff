
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <BaseTDMinionSystemComponent.h>

namespace BaseTDMinion
{
    class BaseTDMinionModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(BaseTDMinionModule, "{0251D8ED-364D-49CA-B2B2-33445B2D135A}", AZ::Module);
        AZ_CLASS_ALLOCATOR(BaseTDMinionModule, AZ::SystemAllocator, 0);

        BaseTDMinionModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                BaseTDMinionSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<BaseTDMinionSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(BaseTDMinion_3e2e046f0f8e48f5bf66f8f03c0ee621, BaseTDMinion::BaseTDMinionModule)
