#pragma once

#include <AzCore/Component/Component.h>

#include <BaseTDMinion/BaseTDMinionBus.h>

namespace BaseTDMinion
{
    class BaseTDMinionSystemComponent
        : public AZ::Component
        , protected BaseTDMinionRequestBus::Handler
    {
    public:
        AZ_COMPONENT(BaseTDMinionSystemComponent, "{56174F5D-E6E5-4FAA-8B5F-34A9CB0E902F}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // BaseTDMinionRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
