#pragma once

#include <AzCore/EBus/EBus.h>

namespace BaseHero
{
    class BaseHeroRequests
        : public AZ::EBusTraits
    {
    public:
        //////////////////////////////////////////////////////////////////////////
        // EBusTraits overrides
        static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
        static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::Single;
        //////////////////////////////////////////////////////////////////////////

    };
    using BaseHeroRequestBus = AZ::EBus<BaseHeroRequests>;
} // namespace BaseHero
