
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <BaseHeroSystemComponent.h>

namespace BaseHero
{
    class BaseHeroModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(BaseHeroModule, "{A854BFED-9662-474D-8133-A1FC2CBFA30E}", AZ::Module);
        AZ_CLASS_ALLOCATOR(BaseHeroModule, AZ::SystemAllocator, 0);

        BaseHeroModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                BaseHeroSystemComponent::CreateDescriptor(),
            });
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(BaseHero_1aa07d1b5eb142d185216f9e9a983ce7, BaseHero::BaseHeroModule)
