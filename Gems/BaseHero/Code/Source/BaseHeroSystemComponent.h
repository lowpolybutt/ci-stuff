#pragma once

#include <AzCore/Component/Component.h>
#include <AzCore/Component/TransformBus.h>
#include <AzCore/Component/TickBus.h>
#include <AzFramework/Input/Events/InputChannelEventListener.h>

#include "../../../../../../Amazon/Lumberyard/1.20.0.0/dev/Gems/Async/Code/Include/Async/AsyncBus.h"
#include <../../../../../../1.20.0.0/dev/Gems/LmbrCentral/Code/include/LmbrCentral/Scripting/TriggerAreaComponentBus.h>

#include <BaseHero/BaseHeroBus.h>

namespace BaseHero
{
	struct Vector3;

	class BaseHeroSystemComponent
		: public AZ::Component
		, public AzFramework::InputChannelEventListener
		, public AZ::TickBus::Handler
		, public AZ::TransformNotificationBus::Handler
		, protected Async::AsyncRequestBus::Handler
        , protected BaseHeroRequestBus::Handler
    {
    public:
        AZ_COMPONENT(BaseHeroSystemComponent, "{BEF3B9E9-0BBF-45CC-A6C3-78A86904EE0D}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // BaseHeroRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;

		bool OnInputChannelEventFiltered(const AzFramework::InputChannel& input) override;

		void OnTick(float, AZ::ScriptTimePoint) override;
        ////////////////////////////////////////////////////////////////////////

	private:
		enum class HeroType
		{
			HeroDps,
			HeroSupport,
			HeroTank
		};

		HeroType heroType;
		AZStd::string charName;
		float hp = 100.f;
		float ultChargeRatePerMinute = 1.f;

		void SetHp(float);
		void SetUltChargeRate(float);

		void OnKeyboardEvent(const AzFramework::InputChannel& input);
		void OnMouseEvent(const AzFramework::InputChannel& input);

		void HandleYAxis(AZ::Vector3 &desiredVelocity);
		void HandleXAxis(AZ::Vector3 &desiredVelocity);
		void HandleZAxis(AZ::Vector3 &desiredVelocity);

		const AZ::Quaternion GetCurrentOrientation();

		float MovementScale = 5.f;

		float JumpHeight = 5.f;

		bool bIsMovingForward = false;
		bool bIsMovingBackward = false;
		bool bIsStrafingLeft = false;
		bool bIsStrafingRight = false;
		bool bIsJumping = false;

		void PerformRotation(const AzFramework::InputChannel &input);
		void TrackMouseMovement(const AzFramework::InputChannel::PositionData2D *position_data);

		void CenterCursorPosition();

		AZ::Vector2 m_lastMousePosition{ .5f, .5f };
		AZ::Vector2 m_mouseChangeAggregate{ 0, 0 };
		float RotationSpeed = 5.f;
    };
}
