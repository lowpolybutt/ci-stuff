
#include <CursorDisplaySystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace CursorDisplay
{
    void CursorDisplaySystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<CursorDisplaySystemComponent, AZ::Component>()
                ->Version(0);

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<CursorDisplaySystemComponent>("CursorDisplay", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
					->Attribute(AZ::Edit::Attributes::Category, "TowerDefence")
                    ;
            }
        }
    }

    void CursorDisplaySystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("CursorDisplayService"));
    }

    void CursorDisplaySystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("CursorDisplayService"));
    }

    void CursorDisplaySystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
		required.push_back(AZ_CRC("UiCanvasRefService"));
    }

    void CursorDisplaySystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

	void CursorDisplaySystemComponent::OnAction(AZ::EntityId id, const LyShine::ActionName& name)
	{
		AZ_Printf("CDSC", "%s", "nfkelqnfkl");
	}

    void CursorDisplaySystemComponent::Init()
    {
    }

    void CursorDisplaySystemComponent::Activate()
    {
        CursorDisplayRequestBus::Handler::BusConnect();
		UiCanvasNotificationBus::Handler::BusConnect(GetEntityId());
		UiCursorBus::Broadcast(&UiCursorInterface::IncrementVisibleCounter);
    }

    void CursorDisplaySystemComponent::Deactivate()
    {
        CursorDisplayRequestBus::Handler::BusDisconnect();
    }

	AZ::EntityId CursorDisplaySystemComponent::GetId()
	{
		AZ::EntityId id;
		UiCanvasRefBus::EventResult(id, GetEntityId(), &UiCanvasRefBus::Events::GetCanvas);
		return id;
	}
}
