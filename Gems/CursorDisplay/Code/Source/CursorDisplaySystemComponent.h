#pragma once

#include <platform_impl.h>

#include <AzCore/Component/Component.h>

#include <LyShine/Bus/UiCursorBus.h>
#include <LyShine/Bus/UiButtonBus.h>
#include <LyShine/Bus/UiCanvasBus.h>
#include <LyShine/Bus/World/UiCanvasRefBus.h>

#include <CursorDisplay/CursorDisplayBus.h>

namespace CursorDisplay
{
    class CursorDisplaySystemComponent
        : public AZ::Component
		, public UiCanvasNotificationBus::Handler
        , protected CursorDisplayRequestBus::Handler
    {
    public:
        AZ_COMPONENT(CursorDisplaySystemComponent, "{55301B40-678A-43B6-BC53-2F0EFE3965DC}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // CursorDisplayRequestBus interface implementation
		void OnAction(AZ::EntityId, const LyShine::ActionName&) override;
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////

	private:
		AZ::EntityId GetId();
    };
}
