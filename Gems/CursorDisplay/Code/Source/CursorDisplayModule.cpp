
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <CursorDisplaySystemComponent.h>

namespace CursorDisplay
{
    class CursorDisplayModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(CursorDisplayModule, "{E3887798-F90C-4F4D-865C-5299053DA037}", AZ::Module);
        AZ_CLASS_ALLOCATOR(CursorDisplayModule, AZ::SystemAllocator, 0);

        CursorDisplayModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                CursorDisplaySystemComponent::CreateDescriptor(),
            });
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(CursorDisplay_3e2fd095e2834695b983ee1d2adc8939, CursorDisplay::CursorDisplayModule)
