#pragma once

#include <AzCore/EBus/EBus.h>
#include <AzCore/Component/EntityId.h>

namespace PlayerController
{
	enum class ActionState
	{
		Started,
		Stopped
	};

    class PlayerControllerRequests
        : public AZ::EBusTraits
    {
    public:
		virtual ~PlayerControllerRequests() = default;
        //////////////////////////////////////////////////////////////////////////
        // EBusTraits overrides
        static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
        static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::Single;
        //////////////////////////////////////////////////////////////////////////

        // Put your public methods here
		virtual void MoveForward(ActionState) {}
		virtual void MoveBackward(ActionState) {}
		virtual void StrafeLeft(ActionState) {}
		virtual void StrafeRight(ActionState) {}

		virtual void LookVertical(float) {}
    };
    using PlayerControllerRequestBus = AZ::EBus<PlayerControllerRequests>;
} // namespace PlayerController
