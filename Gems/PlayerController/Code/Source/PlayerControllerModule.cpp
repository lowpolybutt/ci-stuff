
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <PlayerControllerSystemComponent.h>

namespace PlayerController
{
    class PlayerControllerModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(PlayerControllerModule, "{A50FFB23-12C1-4EDC-B909-0B4CA4780364}", AZ::Module);
        AZ_CLASS_ALLOCATOR(PlayerControllerModule, AZ::SystemAllocator, 0);

        PlayerControllerModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                PlayerControllerSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<PlayerControllerSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(PlayerController_70e8eb554fd54f01be06ef80a8003e50, PlayerController::PlayerControllerModule)
