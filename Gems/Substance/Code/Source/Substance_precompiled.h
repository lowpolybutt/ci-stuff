/*
* All or portions of this file Copyright (c) Amazon.com, Inc. or its affiliates or
* its licensors.
*
* For complete copyright and license terms please see the LICENSE at the root of this
* distribution (the "License"). All use of this software is governed by the License,
* or, if provided, by the license below or the license accompanying this file. Do not
* remove or modify any license notices. This file is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*
*/
/** @file Substance_precompiled.h
    @brief Precompiled Header
    @author Josh Coyne - Allegorithmic (josh.coyne@allegorithmic.com)
    @date 09-14-2015
    @copyright Allegorithmic. All rights reserved.
*/
#ifndef GEM_46AED0DF_955D_4582_9583_0B4D2422A727_CODE_SOURCE_SUBSTANCE_PRECOMPILED_H
#define GEM_46AED0DF_955D_4582_9583_0B4D2422A727_CODE_SOURCE_SUBSTANCE_PRECOMPILED_H
#pragma once

#include <platform.h>

#if defined(AZ_USE_SUBSTANCE)

#define MEM_KB(mem) (mem * 1024)
#define MEM_MB(mem) (MEM_KB(mem) * 1024)

#define DIMOF(x) (sizeof(x) / sizeof(x[0]))

#include <CryWindows.h> // needed for UINT defines used by CImageExtensionHelper
#include <CryName.h>
#include <I3DEngine.h>
#include <ISerialize.h>
#include <IGem.h>

//substance
#include <substance/handle.h>
#include <substance/version.h>
#include <substance/framework/renderer.h>
#include <substance/framework/framework.h>

#include <stdint.h>

#include "Substance/IProceduralMaterial.h"

typedef std::map<SubstanceAir::InputInstanceImage*, string> InputImageMap;

struct ISubstanceAPI;
extern ISubstanceAPI* gAPI;

struct ISubstanceLibAPI;
extern ISubstanceLibAPI* gLibAPI;

//CVars
extern int substance_coreCount;
extern int substance_memoryBudget;

#define DIMOF(x) (sizeof(x) / sizeof(x[0]))
#endif

#endif//GEM_46AED0DF_955D_4582_9583_0B4D2422A727_CODE_SOURCE_SUBSTANCE_PRECOMPILED_H

