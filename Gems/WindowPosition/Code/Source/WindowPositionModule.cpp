
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <CrySystemBus.h>
#include <WindowPositionCommands.h>
#include <platform_impl.h>

namespace WindowPosition
{
    class WindowPositionModule
        : public AZ::Module
		, public CrySystemEventBus::Handler
    {
    public:
        AZ_RTTI(WindowPositionModule, "{34CF496F-3064-4151-9F2E-9114C1298674}", AZ::Module);
        AZ_CLASS_ALLOCATOR(WindowPositionModule, AZ::SystemAllocator, 0);

        WindowPositionModule()
        {
			CrySystemEventBus::Handler::BusConnect();
        }

		~WindowPositionModule()
		{
			CrySystemEventBus::Handler::BusDisconnect();
		}

		WindowPositionCommands m_commands;

		void OnCrySystemInitialized(ISystem &system, const SSystemInitParams&) override
		{
			m_commands.Register(system);
		}

		void OnCrySystemShutdown(ISystem &system) override
		{
			m_commands.Unregister(system);
		}
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(WindowPosition_e05d2ccd0ea741eea3d54627eec47e0f, WindowPosition::WindowPositionModule)
