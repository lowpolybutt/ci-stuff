#pragma once

struct ISystem;
struct IConsoleCmdArgs;

namespace WindowPosition
{
	class WindowPositionCommands
	{
	public:
		void Register(ISystem&);
		void Unregister(ISystem&);

		static void WindowXY(IConsoleCmdArgs*);
	};
}