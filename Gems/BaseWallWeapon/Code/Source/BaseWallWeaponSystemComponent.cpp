
#include <BaseWallWeaponSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace BaseWallWeapon
{
    void BaseWallWeaponSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
			serialize->Class<BaseWallWeaponSystemComponent, AZ::Component>()
				->Version(0)
				->Field("WeaponName", &BaseWallWeaponSystemComponent::weaponName)
				->Field("WeaponBasePrice", &BaseWallWeaponSystemComponent::basePrice)
				->Field("WeaponAmmoPrice", &BaseWallWeaponSystemComponent::ammoPrice)
				->Field("WeaponUpgradedAmmoPrice", &BaseWallWeaponSystemComponent::upgradedAmmoPrice)
				->Field("WeaponRpm", &BaseWallWeaponSystemComponent::rpm)
				->Field("WeaponReserveAmmoCount", &BaseWallWeaponSystemComponent::reserveAmmo)
				->Field("WeaponMagazineCapacity", &BaseWallWeaponSystemComponent::magazineCapacity)
				;

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
				ec->Class<BaseWallWeaponSystemComponent>("BaseWallWeapon", "[Description of functionality provided by this System Component]")
					->ClassElement(AZ::Edit::ClassElements::EditorData, "")
					->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
					->Attribute(AZ::Edit::Attributes::AutoExpand, true)
					->DataElement(AZ::Edit::UIHandlers::Default, &BaseWallWeaponSystemComponent::weaponName, "Weapon Name", "The name of the weapon")
					->DataElement(AZ::Edit::UIHandlers::Default, &BaseWallWeaponSystemComponent::basePrice, "Base price", "The price of the base weapon")
					->DataElement(AZ::Edit::UIHandlers::Default, &BaseWallWeaponSystemComponent::ammoPrice, "Ammo price", "Cost to replenish ammo")
					->DataElement(AZ::Edit::UIHandlers::Default, &BaseWallWeaponSystemComponent::upgradedAmmoPrice, "Upgraded ammo price", "Cost to replenish upgraded ammo")
					->DataElement(AZ::Edit::UIHandlers::Default, &BaseWallWeaponSystemComponent::rpm, "RPM", "The rounds per minute of the weapon")
					->DataElement(AZ::Edit::UIHandlers::Default, &BaseWallWeaponSystemComponent::reserveAmmo, "Reserve ammo", "The amount of ammo kept in reserve at max ammo")
					->DataElement(AZ::Edit::UIHandlers::Default, &BaseWallWeaponSystemComponent::magazineCapacity, "Magazine capacity", "The capacity of one magazine")
                    ;
            }
        }
    }

    void BaseWallWeaponSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("BaseWallWeaponService"));
    }

    void BaseWallWeaponSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("BaseWallWeaponService"));
    }

    void BaseWallWeaponSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void BaseWallWeaponSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

    void BaseWallWeaponSystemComponent::Init()
    {
    }

    void BaseWallWeaponSystemComponent::Activate()
    {
        BaseWallWeaponRequestBus::Handler::BusConnect();
    }

    void BaseWallWeaponSystemComponent::Deactivate()
    {
        BaseWallWeaponRequestBus::Handler::BusDisconnect();
    }
}
