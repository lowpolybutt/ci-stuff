
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <BaseWallWeaponSystemComponent.h>

namespace BaseWallWeapon
{
    class BaseWallWeaponModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(BaseWallWeaponModule, "{4CB1A7F6-9AEB-45CF-BDF6-07AEA22F7F9D}", AZ::Module);
        AZ_CLASS_ALLOCATOR(BaseWallWeaponModule, AZ::SystemAllocator, 0);

        BaseWallWeaponModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                BaseWallWeaponSystemComponent::CreateDescriptor(),
            });
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(BaseWallWeapon_1bb6eec84d7d419fa6e792a9837596d1, BaseWallWeapon::BaseWallWeaponModule)
