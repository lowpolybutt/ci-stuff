#pragma once

#include <AzCore/Component/Component.h>

#include <BaseWallWeapon/BaseWallWeaponBus.h>

namespace BaseWallWeapon
{
    class BaseWallWeaponSystemComponent
        : public AZ::Component
        , protected BaseWallWeaponRequestBus::Handler
    {
    public:
        AZ_COMPONENT(BaseWallWeaponSystemComponent, "{8D3CA46B-44E9-4B86-8C11-6714443F12CD}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // BaseWallWeaponRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////

	private:
		AZStd::string weaponName;
		int basePrice;
		int ammoPrice;
		int upgradedAmmoPrice;
		int rpm;
		int reserveAmmo;
		int magazineCapacity;
    };
}
