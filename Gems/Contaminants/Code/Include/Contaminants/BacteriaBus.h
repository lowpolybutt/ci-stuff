#pragma once

#include <AzCore/Component/EntityId.h>
#include <AzCore/EBus/EBus.h>

namespace Contaminants
{
	class BacteriaRequests
		: public AZ::EBusTraits
	{
	public:
		static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
		static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::ById;

		using BusIdType = AZ::EntityId;

		enum class EBacteriaGramType
		{
			GRAM_POSITIVE,
			GRAM_NEGATIVE
		};

		enum class EBacteriaMotilityType
		{
			MOTILE,
			NON_MOTILE
		};

		struct SBacteria
		{
			AZ_TYPE_INFO(SBacteria, "{7997D625-C35B-4187-A659-BCA4315C8F57}");
			SBacteria()
				: m_name()
				, m_gramType()
				, m_motilityType() {}
			AZStd::string m_name;
			EBacteriaGramType m_gramType;
			EBacteriaMotilityType m_motilityType;
		};
	};
	using BacteriaRequestsBus = AZ::EBus<BacteriaRequests>;
}