#pragma once

#include <Contaminants/BacteriaBus.h>

#include <AzCore/Component/Component.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/SerializeContext.h>

namespace Contaminants
{
	class BacteriaComponent
		: public AZ::Component
		, protected Contaminants::BacteriaRequestsBus::Handler
	{
	public:
		AZ_COMPONENT(BacteriaComponent, "{45AF47C4-CBAC-483A-BAAF-6ACE34432A5B}");

		static void Reflect(AZ::ReflectContext*);

	protected:
		void Init() override {};
		void Activate() override;
		void Deactivate() override;

		SBacteria m_bacteriaDescriptor;
		bool m_isAirborne = false;
	};
}