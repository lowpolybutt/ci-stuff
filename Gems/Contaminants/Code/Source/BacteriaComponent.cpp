#include "BacteriaComponent.h"

namespace Contaminants
{
	void BacteriaComponent::Reflect(AZ::ReflectContext* c)
	{
		if (AZ::SerializeContext* s = azrtti_cast<AZ::SerializeContext*>(c))
		{
			s->Class<SBacteria>()
				->Version(0)
				->Field("Name", &SBacteria::m_name)
				->Field("GramType", &SBacteria::m_gramType)
				->Field("MotilityType", &SBacteria::m_motilityType)
				;

			s->Class<BacteriaComponent, AZ::Component>()
				->Version(2)
				->Field("BacteriaDescriptor", &BacteriaComponent::m_bacteriaDescriptor)
				->Field("IsAirborne", &BacteriaComponent::m_isAirborne)
				;

			if (AZ::EditContext* e = s->GetEditContext())
			{
				e->Class<SBacteria>("Bacteria", "The main struct for bacterial data")
					->ClassElement(AZ::Edit::ClassElements::EditorData, "")
					->Attribute(AZ::Edit::Attributes::AutoExpand, true)
					->DataElement(AZ::Edit::UIHandlers::Default, &SBacteria::m_name, "Bacterium name", "Name of the bacterium")
					->DataElement(AZ::Edit::UIHandlers::ComboBox, &SBacteria::m_gramType, "Gram type", "The Gram type (+ve/-ve) of the bacterium")
						->EnumAttribute(BacteriaComponent::EBacteriaGramType::GRAM_POSITIVE, "Gram positive")
						->EnumAttribute(BacteriaComponent::EBacteriaGramType::GRAM_NEGATIVE, "Gram negative")
					->DataElement(AZ::Edit::UIHandlers::ComboBox, &SBacteria::m_motilityType, "Motility type", "If the bacterium is motile or not")
						->EnumAttribute(BacteriaComponent::EBacteriaMotilityType::MOTILE, "Motile")
						->EnumAttribute(BacteriaComponent::EBacteriaMotilityType::NON_MOTILE, "Non motile")
					;

				e->Class<BacteriaComponent>("BacteriaComponent", "Adds bacteria based contamiantion logic")
					->ClassElement(AZ::Edit::ClassElements::EditorData, "")
						->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
						->Attribute(AZ::Edit::Attributes::AutoExpand, true)
					->DataElement(AZ::Edit::UIHandlers::Default, &BacteriaComponent::m_bacteriaDescriptor, 
						"Bacteria descriptor", "Set up the characteristics of the bacterium")
					->DataElement(AZ::Edit::UIHandlers::CheckBox, &BacteriaComponent::m_isAirborne,
						"Airborne", "Sets if the pathogen is airborne or not")
					;
			}
		}
	}

	void BacteriaComponent::Activate()
	{
		BacteriaRequestsBus::Handler::BusConnect(GetEntityId());
	} 
	
	void BacteriaComponent::Deactivate()
	{
		BacteriaRequestsBus::Handler::BusDisconnect();
	}
}