
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <ContaminantsSystemComponent.h>
#include <BacteriaComponent.h>

namespace Contaminants
{
    class ContaminantsModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(ContaminantsModule, "{2B6E815B-126B-4742-ABBB-66F755A6F65A}", AZ::Module);
        AZ_CLASS_ALLOCATOR(ContaminantsModule, AZ::SystemAllocator, 0);

        ContaminantsModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                ContaminantsSystemComponent::CreateDescriptor(),
				BacteriaComponent::CreateDescriptor(),

            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<ContaminantsSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(Contaminants_1464d7f0993b4196a812f5ab0227ba24, Contaminants::ContaminantsModule)
