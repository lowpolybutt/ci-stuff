#pragma once

#include <AzCore/EBus/EBus.h>
#include <AzCore/Math/Vector3.h>

namespace TDMinionBase
{
    class TDMinionBaseRequests
        : public AZ::EBusTraits
    {
    public:
        //////////////////////////////////////////////////////////////////////////
        // EBusTraits overrides
        static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
        static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::ByIdAndOrdered;
        //////////////////////////////////////////////////////////////////////////
		using BusIdType = AZ::EntityId;
		using BusIdOrderCompare = AZStd::greater<BusIdType>;
        // Put your public methods here
		virtual void OnRayHit(AZ::Vector3, float) = 0;
    };
    using TDMinionBaseRequestBus = AZ::EBus<TDMinionBaseRequests>;
} // namespace TDMinionBase
