
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <TDMinionBaseSystemComponent.h>

namespace TDMinionBase
{
    class TDMinionBaseModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(TDMinionBaseModule, "{07013BCD-8211-4FF3-B3D7-1199046AF872}", AZ::Module);
        AZ_CLASS_ALLOCATOR(TDMinionBaseModule, AZ::SystemAllocator, 0);

        TDMinionBaseModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                TDMinionBaseSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        //AZ::ComponentTypeList GetRequiredSystemComponents() const override
        //{
        //    return AZ::ComponentTypeList{
        //        azrtti_typeid<TDMinionBaseSystemComponent>(),
        //    };
        //}
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(TDMinionBase_8bc852c6b7f94361b1a27d7591888461, TDMinionBase::TDMinionBaseModule)
