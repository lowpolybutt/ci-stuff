
#include <TDMinionBaseSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace TDMinionBase
{
    void TDMinionBaseSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
			serialize->Class<TDMinionBaseSystemComponent, AZ::Component>()
				->Version(0)
				->Field("Direction", &TDMinionBaseSystemComponent::m_direction)
				;

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<TDMinionBaseSystemComponent>("TDMinionBase", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
						->Attribute(AZ::Edit::Attributes::Category, "TowerDefence")
					->DataElement(AZ::Edit::UIHandlers::Vector3, &TDMinionBaseSystemComponent::m_direction)
                    ;
            }
        }
    }

    void TDMinionBaseSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("TDMinionBaseService"));
    }

    void TDMinionBaseSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("TDMinionBaseService"));
    }

    void TDMinionBaseSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void TDMinionBaseSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

    void TDMinionBaseSystemComponent::Init()
    {
		AZ_Printf("TDMBSC", "Hi I'm %s", GetEntityId().ToString());
    }

    void TDMinionBaseSystemComponent::Activate()
    {
        TDMinionBaseRequestBus::Handler::BusConnect(GetEntityId());
		AZ::TickBus::Handler::BusConnect();
		AZ::Vector3& initialV = AZ::Vector3(0.f, 1000.f, 0.f);
		AzFramework::PhysicsComponentRequestBus::Event(GetEntityId(),
			&AzFramework::PhysicsComponentRequestBus::Events::SetVelocity, initialV);

		AZ::Vector3 v;
		AZ::TransformBus::EventResult(v, GetEntityId(), &AZ::TransformBus::Events::GetWorldTranslation);
		AZ_Printf("TDMBSC", "%s: %f, %f, %f", "and I'm at", (float)v.GetX(), (float)v.GetY(), (float)v.GetZ());
    }

    void TDMinionBaseSystemComponent::Deactivate()
    {
		AZ::TickBus::Handler::BusDisconnect();
        TDMinionBaseRequestBus::Handler::BusDisconnect();
    }

	void TDMinionBaseSystemComponent::OnTick(float, AZ::ScriptTimePoint)
	{
		AzFramework::PhysicsComponentRequestBus::Event(GetEntityId(),
			&AzFramework::PhysicsComponentRequestBus::Events::AddImpulse, m_direction);
	}

	void TDMinionBaseSystemComponent::OnRayHit(AZ::Vector3 v, float dirToTurn)
	{
		AZ_Printf("BTDM","I've been hit!: %s I'm turning %f, %f, %f", GetEntityId().ToString(),
			(float)v.GetX(), (float)v.GetY(), (float)v.GetZ());
		AzFramework::PhysicsComponentRequestBus::Event(GetEntityId(),
			&AzFramework::PhysicsComponentRequestBus::Events::SetVelocity, v);
		m_direction = v;
	}
}
