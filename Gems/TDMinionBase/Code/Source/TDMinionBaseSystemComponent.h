#pragma once

#include <AzCore/Component/Component.h>
#include <AzCore/Component/TickBus.h>
#include <AzCore/Component/TransformBus.h>
#include <AzFramework/Physics/PhysicsComponentBus.h>
#include <AzFramework/Physics/PhysicsSystemComponentBus.h>

#include <TDMinionBase/TDMinionBaseBus.h>

namespace TDMinionBase
{
    class TDMinionBaseSystemComponent
        : public AZ::Component
		, public AZ::TickBus::Handler
        , protected TDMinionBaseRequestBus::Handler
    {
    public:
        AZ_COMPONENT(TDMinionBaseSystemComponent, "{7066EEDB-D34D-4626-B3C1-0745F82368D8}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // TDMinionBaseRequestBus interface implementation
		void OnRayHit(AZ::Vector3, float) override;
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////

		void OnTick(float, AZ::ScriptTimePoint) override;

	private:
		AZ::Vector3 m_direction;
    };
}
