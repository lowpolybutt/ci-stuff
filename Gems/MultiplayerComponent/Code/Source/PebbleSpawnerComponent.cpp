#include "PebbleSpawnerComponent.h"
#include <AzCore/Serialization/EditContext.h>
#include <AzFramework/Network/NetBindingHandlerBus.h>
#include <AzFramework/Network/NetworkContext.h>
#include <../../../../../../1.20.0.0/dev/Gems/LmbrCentral/Code/include/LmbrCentral/Scripting/SpawnerComponentBus.h>

using namespace MultiplayerComponent;

class PebbleSpawnerComponent::Chunk
	: public GridMate::ReplicaChunkBase
{
public:
	AZ_CLASS_ALLOCATOR(PebbleSpawnerComponent::Chunk, AZ::SystemAllocator, 0);
	Chunk() 
		: RpcSpawnPebble("SpawnPebble") {}

	static const char* GetChunkName()
	{
		return "PebbleSpawnerComponent::Chunk";
	}

	bool IsReplicaMigratable() override { return true; }

	GridMate::Rpc<GridMate::RpcArg<AZ::Transform>>::BindInterface<PebbleSpawnerComponent,
		&PebbleSpawnerComponent::OnPebbleSpawn> RpcSpawnPebble;
};

void PebbleSpawnerComponent::Activate()
{
	PebbleSpawnerComponentBus::Handler::BusConnect();
}

void PebbleSpawnerComponent::Deactivate()
{
	PebbleSpawnerComponentBus::Handler::BusDisconnect();
}

void PebbleSpawnerComponent::Reflect(AZ::ReflectContext* ctx)
{
	AzFramework::NetworkContext* n_ctx = azrtti_cast<AzFramework::NetworkContext*>(ctx);
	if (n_ctx)
	{
		n_ctx->Class<PebbleSpawnerComponent>()
			->Chunk<PebbleSpawnerComponent::Chunk>()
			->RPC<PebbleSpawnerComponent::Chunk, PebbleSpawnerComponent>(
				"SpawnPebble", &Chunk::RpcSpawnPebble)
			;
	}

	AZ::SerializeContext* s_ctx = azrtti_cast<AZ::SerializeContext*>(ctx);
	if (!s_ctx) return;
	s_ctx->Class<PebbleSpawnerComponent, AZ::Component, AzFramework::NetBindable>()
		->Version(0)
		;

	AZ::EditContext* e_ctx = s_ctx->GetEditContext();
	if (!e_ctx) return;
	e_ctx->Class<PebbleSpawnerComponent>("Pebble Spawner Component", "Spawns pebble projectile at World Transform")
		->ClassElement(AZ::Edit::ClassElements::EditorData, "")
		->Attribute(AZ::Edit::Attributes::Category, "Multiplayer Component")
		->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
		;
}

GridMate::ReplicaChunkPtr PebbleSpawnerComponent::GetNetworkBinding()
{
	PebbleSpawnerComponent::Chunk* replica = 
		GridMate::CreateReplicaChunk<PebbleSpawnerComponent::Chunk>();
	replica->SetHandler(this);
	gmChunk = replica;
	return gmChunk;
}

void PebbleSpawnerComponent::SetNetworkBinding(GridMate::ReplicaChunkPtr chunk)
{
	chunk->SetHandler(this);
	gmChunk = chunk;
}

void PebbleSpawnerComponent::UnbindFromNetwork()
{
	gmChunk->SetHandler(nullptr);
	gmChunk = nullptr;
}

bool PebbleSpawnerComponent::OnPebbleSpawn(AZ::Transform tm, const GridMate::RpcContext&)
{
	LmbrCentral::SpawnerComponentRequestBus::Event(GetEntityId(),
		&LmbrCentral::SpawnerComponentRequestBus::Events::SpawnAbsolute, tm);
	return false;
}

void PebbleSpawnerComponent::SpawnPebbleAt(const AZ::Transform& tm)
{
	if (Chunk* c = 
		static_cast<Chunk*>(gmChunk.get()))
	{
		c->RpcSpawnPebble(tm);
	}
	else
	{
		OnPebbleSpawn(tm, {});
	}
}
