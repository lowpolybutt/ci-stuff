#pragma once

#include <AzCore/Component/Component.h>

#include <MultiplayerComponent/MultiplayerComponentBus.h>

namespace MultiplayerComponent
{
    class MultiplayerComponentSystemComponent
        : public AZ::Component
        , protected MultiplayerComponentRequestBus::Handler
    {
    public:
        AZ_COMPONENT(MultiplayerComponentSystemComponent, "{BC00A3D7-211F-4E28-9F1F-0834F4F6AE1D}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // MultiplayerComponentRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
