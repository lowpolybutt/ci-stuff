#pragma once

#include <AzCore/Component/Component.h>
#include <AzFramework/Input/Events/InputChannelEventListener.h>

namespace MultiplayerComponent
{
	class InputCaptureComponent
		: public AZ::Component
		, public AzFramework::InputChannelEventListener
	{
	public:
		AZ_COMPONENT(InputCaptureComponent, "{49FBC2C0-4074-4ADC-B58B-9AC77EBD48B9}");

		void Activate() override;
		void Deactivate() override;

		static void Reflect(AZ::ReflectContext*);

	protected:
		bool OnInputChannelEventFiltered(const AzFramework::InputChannel&) override;
		bool OnKeyboardEvent(const AzFramework::InputChannel&);
		bool OnMouseEvent(const AzFramework::InputChannel&);

	private:
		void CheckAndUpdateForward(bool);
		void CheckAndUpdateBackward(bool);
		void CheckAndUpdateStrafeLeft(bool);
		void CheckAndUpdateStrafeRight(bool);

		void ShootProjectile(bool);

		bool bIsForwardPressed = false;
		bool bIsBackwardPressed = false;
		bool bIsStrafingLeft = false;
		bool bIsStrafingRight = false;

		bool bIsShooting = false;

		AZ::Vector2 vMouseChangeAggregate{ 0,0 };
	};
}