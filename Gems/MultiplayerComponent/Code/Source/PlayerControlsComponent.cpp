#include "PlayerControlsComponent.h"

#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Math/Vector3.h>
#include <AzCore/Component/TransformBus.h>
#include <AzFramework/Physics/CharacterBus.h>

using namespace MultiplayerComponent;

void PlayerControlsComponent::Activate()
{
	PlayerControlsRequestBus::Handler::BusConnect(GetEntityId());
	AZ::TickBus::Handler::BusConnect();
}

void PlayerControlsComponent::Deactivate()
{
	PlayerControlsRequestBus::Handler::BusDisconnect();
	AZ::TickBus::Handler::BusDisconnect();
}

void PlayerControlsComponent::Reflect(AZ::ReflectContext* ctx)
{
	AZ::SerializeContext* s_ctx = azrtti_cast<AZ::SerializeContext*>(ctx);

	if (!s_ctx) return;

	s_ctx->Class<PlayerControlsComponent, Component>()
		->Version(0)
		->Field("Movement speed", &PlayerControlsComponent::fSpeed)
		->Field("Turning speed", &PlayerControlsComponent::fTurnSpeed)
		->Field("Gravity", &PlayerControlsComponent::fGravity)
		;

	AZ::EditContext* e_ctx = s_ctx->GetEditContext();

	if (!e_ctx) return;

	e_ctx->Class<PlayerControlsComponent>("Player controls", "Controls the player")
		->ClassElement(AZ::Edit::ClassElements::EditorData, "")
		->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
		->Attribute(AZ::Edit::Attributes::Category, "Multiplayer Character")
		->DataElement(nullptr, &PlayerControlsComponent::fSpeed,
			"Movement Speed", "How fast the player moves")
		->DataElement(nullptr, &PlayerControlsComponent::fTurnSpeed,
			"Turn speed", "How fast the character turns")
		->DataElement(nullptr, &PlayerControlsComponent::fGravity,
			"Gravity", "U/s")
		;
}

void PlayerControlsComponent::MoveForward(ActionState st)
{
	bIsForward = st == ActionState::Started;
}

void PlayerControlsComponent::MoveBackward(ActionState st)
{
	bIsBackward = st == ActionState::Started;
}

void PlayerControlsComponent::StrafeLeft(ActionState st)
{
	bIsLeft = st == ActionState::Started;
}

void PlayerControlsComponent::StrafeRight(ActionState st)
{
	bIsRight = st == ActionState::Started;
}

void PlayerControlsComponent::Turn(float amt)
{
	fRotZ = amt * fTurnSpeed;
	AZ_Printf("SR", "%s", "should be setting rot");
	SetRotation();
}

void PlayerControlsComponent::Shoot(ActionState st)
{
	if (st != ActionState::Stopped) return;

	AZ::Transform mLoc;

	AZ::TransformBus::EventResult(mLoc, GetEntityId(),
		&AZ::TransformBus::Events::GetWorldTM);

	const AZ::Quaternion q = AZ::Quaternion::CreateFromTransform(mLoc);
	const AZ::Vector3 d = q * AZ::Vector3::CreateAxisY(1.f);
	mLoc.SetTranslation(mLoc.GetTranslation() + d);

	PebbleSpawnerComponentBus::Broadcast(&PebbleSpawnerComponentBus::Events::SpawnPebbleAt,
		mLoc);
}

void PlayerControlsComponent::SetRotation()
{
	AZ_Printf("SR", "%f", fRotZ)
	AZ::TransformBus::Event(GetEntityId(),
		&AZ::TransformBus::Events::SetLocalRotationQuaternion,
		AZ::Quaternion::CreateRotationZ(fRotZ));
}

void PlayerControlsComponent::OnTick(float d, AZ::ScriptTimePoint pt)
{
	static const AZ::Vector3 y = AZ::Vector3::CreateAxisY(1.f);
	static const AZ::Vector3 x = AZ::Vector3::CreateAxisX(1.f);

	AZ::Vector3 dir{ 0,0,0 };
	
	if (bIsForward)
		dir += y;
	if (bIsBackward)
		dir -= y;
	if (bIsLeft)
		dir -= x;
	if (bIsRight)
		dir += x;

	dir *= fSpeed;

	AZ::Quaternion q = AZ::Quaternion::CreateIdentity();
	AZ::TransformBus::EventResult(q, GetEntityId(),
		&AZ::TransformBus::Events::GetWorldRotationQuaternion);

	dir = q * dir;

	dir += AZ::Vector3::CreateAxisZ(fGravity);

	Physics::CharacterRequestBus::Event(GetEntityId(),
		&Physics::CharacterRequestBus::Events::TryRelativeMove, dir, d);
}