#pragma once

#include <AzCore/Component/Component.h>
#include <MultiplayerComponent/PlayerControlsRequestBus.h>

namespace MultiplayerComponent
{
	class WasdUiStatusComponent
		: AZ::Component
		, public PlayerControlsRequestBus::Handler
	{
	public:
		AZ_COMPONENT(WasdUiStatusComponent, "{2916B3EA-5F27-4F38-AADD-471850C7BBFC}");

		void Activate() override { BusConnect(GetEntityId()); }
		void Deactivate() override { BusDisconnect(); };

		static void Reflect(AZ::ReflectContext*);

	protected:
		void MoveForward(ActionState) override;
}