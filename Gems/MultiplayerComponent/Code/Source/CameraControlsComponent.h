#pragma once

#include <AzCore/Component/Component.h>
#include <MultiplayerComponent/PlayerControlsRequestBus.h>
#include <AzCore/Component/TickBus.h>

namespace MultiplayerComponent
{
	class CameraControlsComponent
		: public AZ::Component
		, public PlayerControlsRequestBus::Handler
	{
	public:
		AZ_COMPONENT(CameraControlsComponent, "{BB2146AA-0FA6-41D6-B5E1-CDA8E55CDD5E}");

		void Activate() override;
		void Deactivate() override;

		static void Reflect(AZ::ReflectContext*);

	protected:
		void LookVertical(float) override;

	private:
		float fLookVerticalSpeed = .6f;
		float fRotY = .0f;

		void SetRotation();
	};
}