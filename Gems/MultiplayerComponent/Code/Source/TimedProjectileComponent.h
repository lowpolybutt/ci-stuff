#pragma once

#include <AzCore/Component/Component.h>
#include <AzCore/Component/TickBus.h>
#include <AzFramework/Physics/RigidBodyBus.h>

namespace MultiplayerComponent
{
	class TimedProjectileComponent
		: public AZ::Component
		, public AZ::TickBus::Handler
		, public Physics::RigidBodyNotificationBus::Handler
	{
	public:
		AZ_COMPONENT(TimedProjectileComponent, "{B9744A38-967E-4B65-B992-F2AE8573A91C}");

		void Activate() override;
		void Deactivate() override;

		static void Reflect(AZ::ReflectContext*);

		static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType&);

	protected:
		void OnTick(float, AZ::ScriptTimePoint) override;

		void OnPhysicsEnabled() override;
		void OnPhysicsDisabled() override;

	private:
		float fLifetime = .0f;
		float fMaxLifeTime = 3.f;
		float fVelocity = 10.f;

		void SetInitialVelocity();
	};
}