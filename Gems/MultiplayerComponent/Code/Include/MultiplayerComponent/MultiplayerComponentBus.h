#pragma once

#include <AzCore/EBus/EBus.h>

namespace MultiplayerComponent
{
    class MultiplayerComponentRequests
        : public AZ::EBusTraits
    {
    public:
        //////////////////////////////////////////////////////////////////////////
        // EBusTraits overrides
        static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
        static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::Single;
        //////////////////////////////////////////////////////////////////////////

        // Put your public methods here
    };
    using MultiplayerComponentRequestBus = AZ::EBus<MultiplayerComponentRequests>;
} // namespace MultiplayerComponent
