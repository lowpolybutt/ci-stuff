#pragma once
#include <AzCore/EBus/EBus.h>
#include <AzCore/Math/Transform.h>

namespace MultiplayerComponent
{
	class PebbleSpawnerComponentRequests
		: public AZ::EBusTraits
	{
	public:
		virtual ~PebbleSpawnerComponentRequests() = default;

		virtual void SpawnPebbleAt(const AZ::Transform&) {}
	};

	using PebbleSpawnerComponentBus = AZ::EBus<PebbleSpawnerComponentRequests>;
}