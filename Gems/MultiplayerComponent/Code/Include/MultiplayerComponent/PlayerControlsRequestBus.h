#pragma once

#include <AzCore/EBus/EBus.h>
#include <AzCore/Component/EntityId.h>

namespace MultiplayerComponent
{
	enum class ActionState
	{
		Started,
		Stopped
	};

	class PlayerControlsRequests
		: public AZ::EBusTraits
	{
	public:
		virtual ~PlayerControlsRequests() = default;

		static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
		static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::ById;
		using BusIdType = AZ::EntityId;

		virtual void MoveForward(ActionState) {}
		virtual void MoveBackward(ActionState) {}

		virtual void StrafeLeft(ActionState) {}
		virtual void StrafeRight(ActionState) {}

		virtual void Turn(float) {}

		virtual void LookVertical(float) {}

		virtual void Shoot(ActionState) {}
	};

	using PlayerControlsRequestBus = AZ::EBus<PlayerControlsRequests>;
} // namespace MultiplayerComponent