#pragma once

#include <platform_impl.h>
#include <IGem.h>

#include <AzCore/Component/Component.h>
#include <AzCore/Component/TransformBus.h>
#include <AzCore/Component/TickBus.h>

#include <AzFramework/Physics/PhysicsComponentBus.h>
#include <AzFramework/Physics/PhysicsSystemComponentBus.h>

#include <TurnPoint/TurnPointBus.h>

namespace TurnPoint
{
    class TurnPointSystemComponent
        : public AZ::Component
		, public AZ::TickBus::Handler
        , protected TurnPointRequestBus::Handler
    {
    public:
        AZ_COMPONENT(TurnPointSystemComponent, "{2EA2436A-C57A-4DE7-8AF8-521F04DA76AA}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // TurnPointRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;

		void OnTick(float, AZ::ScriptTimePoint) override;
        ////////////////////////////////////////////////////////////////////////

	private:
		void CastRay();
		void SetOrigin();
		AZ::Vector3 org = AZ::Vector3::CreateZero();
    };
}
