
#include <TurnPointSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace TurnPoint
{
    void TurnPointSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<TurnPointSystemComponent, AZ::Component>()
                ->Version(0);

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<TurnPointSystemComponent>("TurnPoint", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
					->Attribute(AZ::Edit::Attributes::Category, "Tower Defence")
                    ;
            }
        }
    }

    void TurnPointSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("TurnPointService"));
    }

    void TurnPointSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("TurnPointService"));
    }

    void TurnPointSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void TurnPointSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

    void TurnPointSystemComponent::Init()
    {
    }

    void TurnPointSystemComponent::Activate()
    {
        TurnPointRequestBus::Handler::BusConnect();
		AZ::TickBus::Handler::BusConnect();
    }

    void TurnPointSystemComponent::Deactivate()
    {
        TurnPointRequestBus::Handler::BusDisconnect();
		AZ::TickBus::Handler::BusDisconnect();
    }

	void TurnPointSystemComponent::OnTick(float, AZ::ScriptTimePoint)
	{
		SetOrigin();
		CastRay();
	}

	void TurnPointSystemComponent::CastRay()
	{
		AZ_Printf("TPSC", "Casting ray from origin: %f, %f, %f", org.GetX(), org.GetY(), org.GetZ());
	}

	void TurnPointSystemComponent::SetOrigin()
	{
		AZ::TransformBus::EventResult(org, GetEntityId(), &AZ::TransformBus::Events::GetWorldTranslation);
	}
}
