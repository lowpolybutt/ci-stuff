
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <TurnPointSystemComponent.h>

namespace TurnPoint
{
    class TurnPointModule
        : public CryHooksModule
    {
    public:
        AZ_RTTI(TurnPointModule, "{A5563F77-B103-4887-8D2D-DFA3362D0CFB}",CryHooksModule);
        AZ_CLASS_ALLOCATOR(TurnPointModule, AZ::SystemAllocator, 0);

        TurnPointModule()
            : CryHooksModule()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                TurnPointSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<TurnPointSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(TurnPoint_b66effd49b1647f3b4358c1a97b5482b, TurnPoint::TurnPointModule)
